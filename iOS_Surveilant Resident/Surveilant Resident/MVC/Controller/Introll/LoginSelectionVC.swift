//
//  LoginSelectionVC.swift
//  Surveilant Resident
//
//  Created by Gabani King on 10/08/21.
//

import UIKit

class LoginSelectionVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(true, forKey: "isFinishedIntro")
        UserDefaults.standard.synchronize()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
//        if UIDevice().userInterfaceIdiom == .phone {
//            switch UIScreen.main.nativeBounds.height {
//            case 1136:
//                print("iPhone 5 or 5S or 5C")
//                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
//                let vc = mainStoryboard.instantiateViewController(withIdentifier: "UpdateScreenVC") as! UpdateScreenVC
//                isFromRegister = false
//                self.navigationController?.pushViewController(vc, animated: true)
//            default:
//                print("Unknown")
//                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = mainStoryboard.instantiateViewController(withIdentifier: "UpdateScreenVC") as! UpdateScreenVC
//                isFromRegister = false
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
    }
    
    @IBAction func clickedRequestAccount(_ sender: Any) {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ApartmentSelectionVC") as! ApartmentSelectionVC
                isFromRegister = true
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ApartmentSelectionVC") as! ApartmentSelectionVC
                isFromRegister = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
}
