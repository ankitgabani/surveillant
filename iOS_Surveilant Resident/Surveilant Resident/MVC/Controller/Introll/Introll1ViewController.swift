//
//  Introll1ViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 10/08/21.
//

import UIKit

class Introll1ViewController: UIViewController {
    
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                
            case 1334:
                print("iPhone 6/6S/7/8")
                imgHeight.constant = 410
            case 1792:
                print("iPhone XR/ 11")
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                
            case 2340:
                print("iphone 12 mini")
                
            case 2436:
                print("iPhone X/XS/11 Pro")
                
            case 2532:
                print("iphone 12 pro")
                
            case 2688:
                print("iPhone XS Max/11 Pro Max")
                
            case 2778:
                print("iphone 12 pro max")
                
            default:
                print("Unknown")
            }
        }
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeDown.direction = .left
        self.view.addGestureRecognizer(swipeDown)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                self.navigationController?.popViewController(animated: true)
                
            case .down:
                print("Swiped down")
            case .left:
                print("Swiped left")
                if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                    case 1136:
                        print("iPhone 5 or 5S or 5C")
                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "Introll2ViewController") as! Introll2ViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    default:
                        print("Unknown")
                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "Introll2ViewController") as! Introll2ViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    @IBAction func clickedNExt(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "Introll2ViewController") as! Introll2ViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "Introll2ViewController") as! Introll2ViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
}
