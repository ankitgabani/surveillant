//
//  ComplaintsViewListViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit

class ComplaintsViewListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    var dicDataComplaints: [SRComplaintChatData] = [SRComplaintChatData]()
    
    var arrListComplaints: [SRComplaintChatComplaintConversation] = [SRComplaintChatComplaintConversation]()
    
    
    var strComplaints = ""
    var strSubTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = "\(strSubTitle)-\(appDelagte.dicLoginUserDetails?.name ?? "")"
        
        tblView.delegate = self
        tblView.dataSource = self
        setUp()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callShowCompalintsAPI(id: strComplaints)
    }
    
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListComplaints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dicData = arrListComplaints[indexPath.row]
        
        let author_type = dicData.authorType ?? ""
        
        if author_type == "Resident"
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "ComplaintsReciveCell") as! ComplaintsReciveCell
            
            cell.lblNAme.text = dicData.author.name ?? ""
            
            cell.lblChat.text = dicData.content ?? ""
            
            cell.lblFirstLetter.text = String(dicData.author.name.prefix(1)).uppercased()
            
            return cell
        }
        else
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "ComplaintsReplyCell") as! ComplaintsReplyCell
            
            cell.lblNAme.text = dicData.author.name ?? ""
            
            cell.lblChat.text = dicData.content ?? ""
            
            cell.lblFirstLetter.text = String(dicData.author.name.prefix(1)).uppercased()
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        callResidentClosedAPI()
    }
    
    
    @IBAction func clickedReply(_ sender: Any) {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintsReplyViewController") as! ComplaintsReplyViewController
                vc.strComplaintsID = strComplaints
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintsReplyViewController") as! ComplaintsReplyViewController
                vc.strComplaintsID = strComplaints
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
    }
    
    @IBAction func cllickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - API Call
    func callShowCompalintsAPI(id: String) {
        
        //APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        let url_ = "v1/residents/complaints/\(id)"
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(url_, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let dicData = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let arrcomplaint_conversations = dicData?.value(forKey: "complaint_conversations") as? NSArray
                            
                            self.arrListComplaints.removeAll()
                            for obj in arrcomplaint_conversations! {
                                let dicData = SRComplaintChatComplaintConversation(fromDictionary: (obj as? NSDictionary)!)
                                self.arrListComplaints.append(dicData)
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                            self.arrListComplaints.removeAll()
                            self.tblView.reloadData()
                        }
                        
                    } else {
                        self.arrListComplaints.removeAll()
                        self.tblView.reloadData()
                    }
                    
                } else {
                    self.arrListComplaints.removeAll()
                    self.tblView.reloadData()
                }
                
            } else {
                self.arrListComplaints.removeAll()
                self.tblView.reloadData()
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    // MARK: - API Call
    func callResidentClosedAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["status": "resolved"]
        
        print(param)
        
        let url_ = "v1/residents/complaints/\(strComplaints)/update_status"
                
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPutClose(url_, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
