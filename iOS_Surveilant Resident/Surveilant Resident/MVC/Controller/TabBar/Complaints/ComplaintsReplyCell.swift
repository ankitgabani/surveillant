//
//  ComplaintsReplyCell.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit

class ComplaintsReplyCell: UITableViewCell {
    
    @IBOutlet weak var lblFirstLetter: UILabel!
    @IBOutlet weak var lblNAme: UILabel!
    @IBOutlet weak var lblChat: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
