//
//  ComplaintsViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit

class ComplaintsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var viewNoData: UIView!
    var arrListComplaints: [SRListComplaintsData] = [SRListComplaintsData]()
    var arrListComplaintsSearching: [SRListComplaintsData] = [SRListComplaintsData]()
    
    var isSearching : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewNoData.isHidden = true
        viewSearch.isHidden = false
        self.txtSearch.delegate = self
        self.txtSearch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
        
        tblView.delegate = self
        tblView.dataSource = self
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callGetCompalintsAPI()

    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
        
        if txtSearch.text == ""
        {
            self.isSearching = false
           
            // reload table view
            self.tblView.reloadData()
        }
        else
        {
            self.isSearching = true
            
            self.arrListComplaintsSearching.removeAll(keepingCapacity: false)
            
            for i in 0..<self.arrListComplaints.count {
                
                let listItem: SRListComplaintsData = self.arrListComplaints[i]
                if listItem.subject.range(of: self.txtSearch.text!) != nil || listItem.descriptionField.range(of: self.txtSearch.text!) != nil
                {
                    self.arrListComplaintsSearching.append(listItem)
                }
            }
           
            self.tblView.reloadData()
        }
                
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching == true {
            return self.arrListComplaintsSearching.count
        }else {
            return self.arrListComplaints.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComplaintsTableCell") as! ComplaintsTableCell
        
        
        var dicData: SRListComplaintsData!
        
        if self.isSearching == true {
            dicData = arrListComplaintsSearching[indexPath.row]
        }else {
            dicData = arrListComplaints[indexPath.row]
        }
        
        let strSubject = dicData.subject
        let strCode = dicData.complaintCode
        
        let name = dicData.complaintType.name ?? ""
        cell.lblType.text = "\(name) :".firstUppercased
        
        cell.lblDes.text = dicData.descriptionField ?? ""
        
        cell.lblSubject.text = strSubject ?? ""
        cell.lblCode.text = strCode ?? ""
        
        let created_at = dicData.createdAt ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: created_at)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd MMM, h:mm a"
        let dayInWeek = dateFormatter2.string(from: date!)
        cell.lblDateTime.text = dayInWeek
        
        
        let strStatus = dicData.status ?? ""
        
        if strStatus == "pending" || strStatus == "escalate" || strStatus == "open"
        {
            cell.lblStatus.text = "Open"
            cell.lblStatus.backgroundColor = RED_Light_COLOR
            cell.lblStatus.borderColor = RED_COLOR
            cell.lblStatus.textColor = RED_COLOR
        }
        else if strStatus == "inprogress" || strStatus == "in progress"
        {
            cell.lblStatus.text = "In progress"
            cell.lblStatus.backgroundColor = PISTA_Light_COLOR
            cell.lblStatus.borderColor = PISTA_COLOR
            cell.lblStatus.textColor = PISTA_COLOR
        }
        else
        {
            cell.lblStatus.text = "Closed"
            cell.lblStatus.backgroundColor = GREEN_Light_COLOR
            cell.lblStatus.borderColor = GREEN_COLOR
            cell.lblStatus.textColor = GREEN_COLOR
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 152
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dicData: SRListComplaintsData!
        
        if self.isSearching == true {
            dicData = arrListComplaintsSearching[indexPath.row]
        }else {
            dicData = arrListComplaints[indexPath.row]
        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintsViewListViewController") as! ComplaintsViewListViewController
                vc.strComplaints = dicData.id ?? ""
                vc.strSubTitle = dicData.complaintCode ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintsViewListViewController") as! ComplaintsViewListViewController
                vc.strComplaints = dicData.id ?? ""
                vc.strSubTitle = dicData.complaintCode ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func clickedAddNew(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddComplaintsViewController") as! AddComplaintsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddComplaintsViewController") as! AddComplaintsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    // MARK: - API Call
    func callGetCompalintsAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(LIST_COMPLAINTS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrListComplaints.removeAll()
                            for obj in arrData! {
                                let dicData = SRListComplaintsData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrListComplaints.append(dicData)
                            }
                            
                            if self.arrListComplaints.count == 0
                            {
                                self.viewNoData.isHidden = false
                                self.viewSearch.isHidden = true
                            }
                            else
                            {
                                self.viewNoData.isHidden = true
                                self.viewSearch.isHidden = false
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                            self.arrListComplaints.removeAll()
                            if self.arrListComplaints.count == 0
                            {
                                self.viewNoData.isHidden = false
                                self.viewSearch.isHidden = true
                            }
                            else
                            {
                                self.viewNoData.isHidden = true
                                self.viewSearch.isHidden = false
                            }
                            
                            self.tblView.reloadData()
                        }
                        
                    } else {
                        self.arrListComplaints.removeAll()
                        if self.arrListComplaints.count == 0
                        {
                            self.viewNoData.isHidden = false
                            self.viewSearch.isHidden = true
                        }
                        else
                        {
                            self.viewNoData.isHidden = true
                            self.viewSearch.isHidden = false
                        }
                        
                        self.tblView.reloadData()
                    }
                    
                } else {
                    self.arrListComplaints.removeAll()
                    if self.arrListComplaints.count == 0
                    {
                        self.viewNoData.isHidden = false
                        self.viewSearch.isHidden = true
                    }
                    else
                    {
                        self.viewNoData.isHidden = true
                        self.viewSearch.isHidden = false
                    }
                    
                    self.tblView.reloadData()
                }
                
            } else {
                self.arrListComplaints.removeAll()
                if self.arrListComplaints.count == 0
                {
                    self.viewNoData.isHidden = false
                    self.viewSearch.isHidden = true
                }
                else
                {
                    self.viewNoData.isHidden = true
                    self.viewSearch.isHidden = false
                }
                
                self.tblView.reloadData()
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    //MARK:- Action MEthod
    
    @IBAction func clickedHome(_ sender: Any) {
        appDelagte.setUpHome()
    }
    
    @IBAction func clickedPayment(_ sender: Any) {
        appDelagte.setUpPayemnt()
    }
    
    @IBAction func clickedChat(_ sender: Any) {
    }
    
    @IBAction func clickedProfile(_ sender: Any) {
        appDelagte.setUpProfile()
    }
    
}
extension StringProtocol {
    var firstUppercased: String { return prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { return prefix(1).capitalized + dropFirst() }
}
