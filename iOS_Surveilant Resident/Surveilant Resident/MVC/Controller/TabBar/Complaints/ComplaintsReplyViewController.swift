//
//  ComplaintsReplyViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit
import MaterialComponents

class ComplaintsReplyViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var txtAddReply: MDCOutlinedTextArea!
    @IBOutlet weak var btnSubit: UIButton!
    
    @IBOutlet weak var viewPopup: UIView!
    
    var arrStatusType = ["escalate","inprogress","resolved"]
    
    let dropDown = DropDown()
    
    var strComplaintsID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopup.isHidden = true
        
        if txtAddReply.textView.text == ""
        {
            btnSubit.alpha = 0.5
            btnSubit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubit.alpha = 1
            btnSubit.isUserInteractionEnabled = true
        }
        
        
        txtAddReply.textView.delegate = self
        
        txtAddReply.label.text = "Add Reply"
        txtAddReply.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtAddReply.placeholder = "Add Reply"
        txtAddReply.textView.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtAddReply.leadingAssistiveLabel.text = "This field is required"
        //        txtAddReply.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtAddReply.setLeadingAssistiveLabel(RED_COLOR, for: .editing)
        txtAddReply.setLeadingAssistiveLabel(RED_COLOR, for: .disabled)
        txtAddReply.setLeadingAssistiveLabel(RED_COLOR, for: .normal)
        
        txtAddReply.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtAddReply.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtAddReply.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtAddReply.setFloatingLabel(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtAddReply.setFloatingLabel(GRAY_COLOR, for: .disabled)
        txtAddReply.setFloatingLabel(GRAY_COLOR, for: .normal)
        
        txtAddReply.containerRadius = 15
        txtAddReply.leadingEdgePaddingOverride = 25
        
        setUp()
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
       if txtAddReply.textView.text == ""
        {
            btnSubit.alpha = 0.5
            btnSubit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubit.alpha = 1
            btnSubit.isUserInteractionEnabled = true
        }
        
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtAddReply.textView.text == ""
        {
            btnSubit.alpha = 0.5
            btnSubit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubit.alpha = 1
            btnSubit.isUserInteractionEnabled = true
        }
    }
    
    
    @objc func clickedOpenDrop()
    {
        dropDown.show()
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickedStatus(_ sender: Any) {
        
        self.view.endEditing(true)
        dropDown.show()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickedSubnit(_ sender: Any) {
        self.view.endEditing(true)
        callResidentReplyAPI()
    }
    
    @IBAction func clickedCancelPopup(_ sender: Any) {
        viewPopup.isHidden = true
    }
    
    
    // MARK: - API Call
    func callResidentReplyAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["content": txtAddReply.textView.text!,"status": ""]
        
        print(param)
        
        let url_ = "v1/residents/complaints/\(strComplaintsID)/conversations"
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPOST(url_, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let dicData = responseUser.value(forKey: "data") as? NSDictionary
                            
                            self.viewPopup.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.7, execute: {
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
