//
//  ComplaintsTableCell.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit

class ComplaintsTableCell: UITableViewCell {

    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var lblSubject: UILabel!
    
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
