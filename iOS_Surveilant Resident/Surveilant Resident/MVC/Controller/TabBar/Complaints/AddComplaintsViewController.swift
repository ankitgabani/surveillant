//
//  AddComplaintsViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit
import MaterialComponents

class AddComplaintsViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var btnrop: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtNAme: MDCOutlinedTextField!
    @IBOutlet weak var txtSubject: MDCOutlinedTextField!
    @IBOutlet weak var txtYourCompints: MDCOutlinedTextArea!
    
    @IBOutlet weak var viewTarget: UIView!
    
    var arrListComplaints: [SRComplaintTypeListData] = [SRComplaintTypeListData]()
    
    let dropDown = DropDown()
    var complaint_type_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callComplaintsTypeListAPI()
        
        if txtNAme.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtSubject.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubmit.alpha = 1
            btnSubmit.isUserInteractionEnabled = true
        }
        
        txtNAme.delegate = self
        txtSubject.delegate = self
        
        self.txtNAme.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtSubject.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        
        txtSubject.label.text = "Complaint Type*"
        txtSubject.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtSubject.placeholder = "Complaint Type*"
        txtSubject.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtSubject.leadingAssistiveLabel.text = "This field is required"
        //        txtSubject.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtSubject.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtSubject.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtSubject.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtSubject.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtSubject.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtSubject.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtSubject.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtSubject.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtSubject.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtSubject.containerRadius = 15
        txtSubject.leadingEdgePaddingOverride = 25
        txtSubject.tintColor = UIColor.clear
        txtSubject.addTarget(self, action: #selector(clickedOpenDrop), for: .editingDidBegin)
        
        
        txtNAme.label.text = "Subject*"
        txtNAme.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtNAme.placeholder = "Subject*"
        txtNAme.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtNAme.leadingAssistiveLabel.text = "This field is required"
        //        txtNAme.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtNAme.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtNAme.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtNAme.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtNAme.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNAme.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtNAme.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtNAme.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNAme.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtNAme.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtNAme.containerRadius = 15
        txtNAme.leadingEdgePaddingOverride = 25
        txtNAme.keyboardType = .default
        
        
        txtYourCompints.textView.textContainer.maximumNumberOfLines = 4
        txtYourCompints.textView.textContainer.lineBreakMode = .byTruncatingTail
        
        txtYourCompints.label.text = "Description*"
        
        txtYourCompints.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtYourCompints.placeholder = "Description*"
        txtYourCompints.textView.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtYourCompints.leadingAssistiveLabel.text = "This field is required"
        //        txtYourCompints.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtYourCompints.setLeadingAssistiveLabel(RED_COLOR, for: .editing)
        txtYourCompints.setLeadingAssistiveLabel(RED_COLOR, for: .disabled)
        txtYourCompints.setLeadingAssistiveLabel(RED_COLOR, for: .normal)
        
        txtYourCompints.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtYourCompints.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtYourCompints.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtYourCompints.setFloatingLabel(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtYourCompints.setFloatingLabel(GRAY_COLOR, for: .disabled)
        txtYourCompints.setFloatingLabel(GRAY_COLOR, for: .normal)
        
        txtYourCompints.containerRadius = 15
        txtYourCompints.leadingEdgePaddingOverride = 25
        
        txtYourCompints.textView.delegate = self
        self.txtYourCompints.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        
        // Do any additional setup after loading the view.
        setUp()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtNAme.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtSubject.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtYourCompints.textView.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubmit.alpha = 1
            btnSubmit.isUserInteractionEnabled = true
        }
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        if txtNAme.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtSubject.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtYourCompints.textView.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubmit.alpha = 1
            btnSubmit.isUserInteractionEnabled = true
        }
        
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func setDropDown() {
        
        let arrName = NSMutableArray()
        
        for object in arrListComplaints
        {
            arrName.add(object.name.uppercased() ?? "")
        }
        
        dropDown.dataSource = arrName as! [String]
        dropDown.anchorView = viewTarget
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            txtSubject.text = item
            txtSubject.resignFirstResponder()
            
            for object in arrListComplaints
            {
                if object.name.uppercased() == item
                {
                    complaint_type_id = object.id
                }
            }
            
            if txtNAme.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else if txtSubject.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else if txtYourCompints.textView.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else
            {
                btnSubmit.alpha = 1
                btnSubmit.isUserInteractionEnabled = true
            }
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor(red: 134/255, green: 134/255, blue: 134/255, alpha: 1)
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.textFont = UIFont(name: "Sk-Modernist-Regular", size: 16)!
        
        dropDown.reloadAllComponents()
    }
    
    @objc func clickedOpenDrop()
    {
        dropDown.show()
    }
    
    @IBAction func clickedBac(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSubmit(_ sender: Any) {
        self.view.endEditing(true)
        callCreateComplaintsAPI()
    }
    
    @IBAction func clickedDropDown(_ sender: Any) {
        txtSubject.becomeFirstResponder()
        self.view.endEditing(true)
        dropDown.show()
    }
    
    
    // MARK: - API Call
    func callComplaintsTypeListAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(COMPLAINTS_TYPE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrListComplaints.removeAll()
                            for obj in arrData! {
                                let dicData = SRComplaintTypeListData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrListComplaints.append(dicData)
                            }
                            
                            self.setDropDown()
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callCreateComplaintsAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let dicData = NSMutableDictionary()
        
        dicData.setValue(txtNAme.text!, forKey: "subject")
        dicData.setValue(txtYourCompints.textView.text!, forKey: "description")
        dicData.setValue(complaint_type_id, forKey: "complaint_type_id")
        dicData.setValue(txtSubject.text!, forKey: "type")
        
        
        let param = ["complaint": dicData]
        
        print(param)
        
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPOST(CREATE_COMPLAINTS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            if UIDevice().userInterfaceIdiom == .phone {
                                switch UIScreen.main.nativeBounds.height {
                                case 1136:
                                    print("iPhone 5 or 5S or 5C")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintNotedVC") as! ComplaintNotedVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                default:
                                    print("Unknown")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintNotedVC") as! ComplaintNotedVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
