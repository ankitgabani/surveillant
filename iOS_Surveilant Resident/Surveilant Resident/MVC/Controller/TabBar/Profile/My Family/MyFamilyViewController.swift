//
//  MyFamilyViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 18/08/21.
//

import UIKit

class MyFamilyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewTableView: UIView!
    
    var arrFamilyMemberList: [FamilyMembersData] = [FamilyMembersData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewNoData.isHidden = true
        viewTableView.isHidden = false
        
        tblView.delegate = self
        tblView.dataSource = self
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.callMemberAPI(showIndicator: true)
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedAddFamaily(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddFamilyViewController") as! AddFamilyViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddFamilyViewController") as! AddFamilyViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFamilyMemberList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "MyFamilyTableCell") as! MyFamilyTableCell
        
        let dicData = arrFamilyMemberList[indexPath.row]
        
        cell.lblName.text = dicData.name ?? ""
        cell.lblPhone.text = dicData.phone ?? ""
        
        cell.lblStatus.text = dicData.category ?? ""
        
        DispatchQueue.main.async {
            cell.viewTop.clipsToBounds = true
            cell.viewTop.layer.cornerRadius = 10
            cell.viewTop.layer.maskedCorners = [.layerMinXMinYCorner, . layerMinXMaxYCorner]
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(clickedDelete(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickedDelete(sender: UIButton)
    {
        let dicData = arrFamilyMemberList[sender.tag]
        callDeleteVehicalAPI(phone: dicData.phone ?? "")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 151
    }
    
    
    // MARK: - API Call
    func callMemberAPI(showIndicator :Bool) {
        
        if showIndicator == true
        {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(MEMBER_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrFamilyMemberList.removeAll()
                            for obj in arrData! {
                                let dicData = FamilyMembersData(fromDictionary: (obj as? NSDictionary)!)
                                
                                if dicData.status == "active"
                                {
                                    self.arrFamilyMemberList.append(dicData)
                                }
                            }
                            
                            self.tblView.reloadData()
                            
                            if self.arrFamilyMemberList.count == 0
                            {
                                self.viewNoData.isHidden = false
                                self.viewTableView.isHidden = true
                            }
                            else
                            {
                                self.viewNoData.isHidden = true
                                self.viewTableView.isHidden = false
                            }
                            
                           
                        } else {
                            self.viewNoData.isHidden = false
                            self.viewTableView.isHidden = true
                        }
                        
                    } else {
                        
                        self.viewNoData.isHidden = false
                        self.viewTableView.isHidden = true
                    }
                    
                } else {
                    
                    self.viewNoData.isHidden = false
                    self.viewTableView.isHidden = true
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callDeleteVehicalAPI(phone: String) {
        
        let param = ["phone": phone]
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderDELETE(DELETE_MEMBER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.callMemberAPI(showIndicator: false)
                            
                        } else {
                            self.viewNoData.isHidden = false
                            self.viewTableView.isHidden = true
                        }
                        
                    } else {
                        
                        self.viewNoData.isHidden = false
                        self.viewTableView.isHidden = true
                    }
                    
                } else {
                    
                    self.viewNoData.isHidden = false
                    self.viewTableView.isHidden = true
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
