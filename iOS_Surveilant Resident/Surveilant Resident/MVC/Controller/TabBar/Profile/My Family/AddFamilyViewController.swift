//
//  AddFamilyViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit
import MaterialComponents

class AddFamilyViewController: UIViewController,UITextFieldDelegate{
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtName: MDCOutlinedTextField!
    @IBOutlet weak var txtPhone: MDCOutlinedTextField!
    @IBOutlet weak var txtEmail: MDCOutlinedTextField!
    @IBOutlet weak var txtRelation: MDCOutlinedTextField!
    
    @IBOutlet weak var viewTarget: UIView!
    
    
    @IBOutlet weak var viewSucuessPop: UIView!
    @IBOutlet weak var lblErrorMsg: UILabel!
    @IBOutlet weak var imgErroPop: UIImageView!
    
    var arrVisitorType = ["Friend","Family"]
    
    let dropDown = DropDown()
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSucuessPop.isHidden = true
        
        if txtName.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else if txtPhone.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
//        else if txtEmail.text == ""
//        {
//            btnSave.alpha = 0.5
//            btnSave.isUserInteractionEnabled = false
//        }
        else if txtRelation.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else
        {
            btnSave.alpha = 1
            btnSave.isUserInteractionEnabled = true
        }
        
        txtName.delegate = self
        txtPhone.delegate = self
        txtEmail.delegate = self
        txtRelation.delegate = self
        
        self.txtName.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtPhone.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtEmail.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtRelation.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        
        
        setDropDown()
        
        txtName.label.text = "Name*"
        txtName.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtName.placeholder = "Name*"
        txtName.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtName.leadingAssistiveLabel.text = "This field is required"
        //        txtName.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtName.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtName.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtName.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtName.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtName.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtName.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtName.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtName.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtName.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtName.containerRadius = 15
        txtName.leadingEdgePaddingOverride = 25
        txtName.keyboardType = .default
        
        
        
        txtPhone.label.text = "Mobile No*"
        txtPhone.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtPhone.placeholder = "Mobile No*"
        txtPhone.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtPhone.leadingAssistiveLabel.text = "This field is required"
        //        txtPhone.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtPhone.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtPhone.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtPhone.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtPhone.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPhone.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtPhone.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtPhone.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPhone.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtPhone.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtPhone.containerRadius = 15
        txtPhone.leadingEdgePaddingOverride = 25
        txtPhone.keyboardType = .phonePad
        
        
        txtEmail.label.text = "Email Id"
        txtEmail.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtEmail.placeholder = "Email Id"
        txtEmail.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtEmail.leadingAssistiveLabel.text = "This field is required"
        //        txtEmail.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtEmail.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtEmail.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtEmail.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtEmail.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtEmail.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmail.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtEmail.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtEmail.containerRadius = 15
        txtEmail.leadingEdgePaddingOverride = 25
        txtEmail.keyboardType = .emailAddress
        
        
        
        txtRelation.label.text = "Relation*"
        txtRelation.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtRelation.placeholder = "Relation*"
        txtRelation.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtRelation.leadingAssistiveLabel.text = "This field is required"
        //        txtRelation.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtRelation.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtRelation.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtRelation.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtRelation.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtRelation.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtRelation.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtRelation.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtRelation.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtRelation.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtRelation.containerRadius = 15
        txtRelation.leadingEdgePaddingOverride = 25
        txtRelation.tintColor = UIColor.clear
        txtRelation.addTarget(self, action: #selector(clickedOpenDrop), for: .editingDidBegin)
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtName.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else if txtPhone.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
//        else if txtEmail.text == ""
//        {
//            btnSave.alpha = 0.5
//            btnSave.isUserInteractionEnabled = false
//        }
        else if txtRelation.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else
        {
            btnSave.alpha = 1
            btnSave.isUserInteractionEnabled = true
        }
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
    }
    
    
    func setDropDown() {
        
        dropDown.dataSource = arrVisitorType
        dropDown.anchorView = viewTarget
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            txtRelation.text = item
            txtRelation.resignFirstResponder()
            
            if txtName.text == ""
            {
                btnSave.alpha = 0.5
                btnSave.isUserInteractionEnabled = false
            }
            else if txtPhone.text == ""
            {
                btnSave.alpha = 0.5
                btnSave.isUserInteractionEnabled = false
            }
//            else if txtEmail.text == ""
//            {
//                btnSave.alpha = 0.5
//                btnSave.isUserInteractionEnabled = false
//            }
            else if txtRelation.text == ""
            {
                btnSave.alpha = 0.5
                btnSave.isUserInteractionEnabled = false
            }
            else
            {
                btnSave.alpha = 1
                btnSave.isUserInteractionEnabled = true
            }
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor(red: 134/255, green: 134/255, blue: 134/255, alpha: 1)
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.textFont = UIFont(name: "Sk-Modernist-Regular", size: 16)!
        dropDown.reloadAllComponents()
    }
    
    @objc func clickedOpenDrop()
    {
        dropDown.show()
    }
    
    @IBAction func clickedSelectRelation(_ sender: Any) {
        txtRelation.becomeFirstResponder()
        self.view.endEditing(true)
        dropDown.show()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPhone {
            let newLength = (textField.text?.utf16.count)! + string.utf16.count - range.length
            if newLength <= 10 {
                let allowedCharacters = CharacterSet.decimalDigits
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            } else {
                return false
            }
        }
        else if textField == txtName
        {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")

            return (string == filtered)
        }
        else
        {
            return true
        }
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSave(_ sender: Any) {
        self.view.endEditing(true)
        callCreateMemberAPI()
      
    }
    
    @IBAction func clickedCancelPop(_ sender: Any) {
        self.viewSucuessPop.isHidden = true
    }
    
    
    // MARK: - API Call
    func callCreateMemberAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["phone": txtPhone.text ?? "","name": txtName.text ?? "","email": txtEmail.text ?? "","category": txtRelation.text?.lowercased() ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPOST(CREATE_MEMBER, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                let arrData = (response?["data"] as? NSArray)?[0] as? String
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            self.viewSucuessPop.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewSucuessPop.isHidden = true
                            }
                            
                            self.viewSucuessPop.backgroundColor = GREEN_Light_COLOR
                            self.viewSucuessPop.borderColor = GREEN_COLOR
                            self.lblErrorMsg.textColor = GREEN_COLOR
                            
                            
                            self.imgErroPop.image = UIImage(named: "ic_Success_Msg")
                            self.lblErrorMsg.text = "Saved Successfully..!"
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                        else
                        {
                            self.viewSucuessPop.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewSucuessPop.isHidden = true
                            }
                            
                            self.viewSucuessPop.backgroundColor = RED_Light_COLOR
                            self.viewSucuessPop.borderColor = RED_COLOR
                            self.lblErrorMsg.textColor = RED_COLOR
                            
                            
                            self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                            self.lblErrorMsg.text = arrData
                        }
                        
                    } else {
                        
                        self.viewSucuessPop.isHidden = false
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewSucuessPop.isHidden = true
                        }
                        
                        self.viewSucuessPop.backgroundColor = RED_Light_COLOR
                        self.viewSucuessPop.borderColor = RED_COLOR
                        self.lblErrorMsg.textColor = RED_COLOR
                        
                        
                        self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                        self.lblErrorMsg.text = arrData
                    }
                    
                } else  if statusCode == 201 {
                    
                    if status == 1
                    {
                        self.viewSucuessPop.isHidden = false
                        self.viewSucuessPop.backgroundColor = GREEN_Light_COLOR
                        
                        self.viewSucuessPop.borderColor = GREEN_COLOR
                        self.lblErrorMsg.textColor = GREEN_COLOR
                        
                        
                        self.imgErroPop.image = UIImage(named: "ic_Success_Msg")
                        self.lblErrorMsg.text = "Saved Successfully..!"
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else
                    {
                        self.viewSucuessPop.isHidden = false
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewSucuessPop.isHidden = true
                        }
                        
                        self.viewSucuessPop.backgroundColor = RED_Light_COLOR
                        self.viewSucuessPop.borderColor = RED_COLOR
                        self.lblErrorMsg.textColor = RED_COLOR
                        
                        
                        self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                        self.lblErrorMsg.text = arrData
                    }
                    
                }
                else
                {
                    self.viewSucuessPop.isHidden = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewSucuessPop.isHidden = true
                    }
                    
                    self.viewSucuessPop.backgroundColor = RED_Light_COLOR
                    self.viewSucuessPop.borderColor = RED_COLOR
                    self.lblErrorMsg.textColor = RED_COLOR
                    
                    
                    self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                    self.lblErrorMsg.text = arrData
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
