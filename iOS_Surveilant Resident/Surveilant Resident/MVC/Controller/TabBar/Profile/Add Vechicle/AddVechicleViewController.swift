//
//  AddVechicleViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit

class AddVechicleViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewData: UIView!
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 30.0,
                                     bottom: 0.0,
                                     right: 30.0)
    let itemsPerRow: CGFloat = 2
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 177)
        _flowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 30.0, bottom: 0.0, right: 30.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 15
        return _flowLayout
    }
    
    let sectionInsets1 = UIEdgeInsets(top: 0.0,
                                      left: 15.0,
                                      bottom: 0.0,
                                      right: 15.0)
    let itemsPerRow1: CGFloat = 2
    
    var flowLayout1: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets1.left * (itemsPerRow1 + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow1
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 170)
        _flowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 15.0, bottom: 0.0, right: 15.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 7
        return _flowLayout
    }
        
    var arrVehicleList: [VehiclesListData] = [VehiclesListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        viewNoData.isHidden = true
        viewData.isHidden = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                self.collectionView.collectionViewLayout = flowLayout1
            default:
                print("Unknown")
                self.collectionView.collectionViewLayout = flowLayout1
            }
        }
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callVehicalAPI(showIndicator: true)
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedAddVehicle(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddNewVehicleViewController") as! AddNewVehicleViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddNewVehicleViewController") as! AddNewVehicleViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrVehicleList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddVechicleCollectionCell", for: indexPath) as! AddVechicleCollectionCell
        
        let dicData = arrVehicleList[indexPath.row]

        let type = dicData.vehicleType ?? ""

        cell.lblType.text = type
        cell.lblPLte.text = dicData.numberPlate ?? ""
        
        if type == "car"
        {
            cell.imgVEhicle.image = UIImage(named: "ic_car")
        }
        else
        {
            cell.imgVEhicle.image = UIImage(named: "ic_bike")
        }
        
        cell.btnCancel.tag = indexPath.row
        cell.btnCancel.addTarget(self, action: #selector(clickedCancel(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickedCancel(sender: UIButton)
    {
        let dicData = arrVehicleList[sender.tag]

        callDeleteVehicalAPI(vehicle_id: dicData.id ?? "")
    }
        
    // MARK: - API Call
    func callVehicalAPI(showIndicator :Bool) {
        
        if showIndicator == true
        {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["resident_id": appDelagte.dicLoginUserDetails?.id ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(VEHICLE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrVehicleList.removeAll()
                            for obj in arrData! {
                                let dicData = VehiclesListData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrVehicleList.append(dicData)
                            }
                            
                            self.collectionView.reloadData()
                            
                            if self.arrVehicleList.count == 0
                            {
                                self.viewNoData.isHidden = false
                                self.viewData.isHidden = true
                            }
                            else
                            {
                                self.viewNoData.isHidden = true
                                self.viewData.isHidden = false
                            }
                            
                            
                        } else {
                            self.viewNoData.isHidden = false
                            self.viewData.isHidden = true
                        }
                        
                    } else {
                        
                        self.viewNoData.isHidden = false
                        self.viewData.isHidden = true
                    }
                    
                } else {
                    
                    self.viewNoData.isHidden = false
                    self.viewData.isHidden = true
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callDeleteVehicalAPI(vehicle_id: String) {
                
        let url_ = "/v1/residents/vehicles/\(vehicle_id)/owner"
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderDELETE(url_, parameters: [:], completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                           
                            self.callVehicalAPI(showIndicator: false)
                           
                        } else {
                            self.viewNoData.isHidden = false
                            self.viewData.isHidden = true
                        }
                        
                    } else {
                        
                        self.viewNoData.isHidden = false
                        self.viewData.isHidden = true
                    }
                    
                } else {
                    
                    self.viewNoData.isHidden = false
                    self.viewData.isHidden = true
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
