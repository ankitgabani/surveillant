//
//  AddNewVehicleViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 19/08/21.
//

import UIKit
import MaterialComponents

class AddNewVehicleViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtVehicleType: MDCOutlinedTextField!
    @IBOutlet weak var txtNumber: MDCOutlinedTextField!
    
    @IBOutlet weak var viewTqrget: UIView!
    @IBOutlet weak var lblErrorMsg: UILabel!
    @IBOutlet weak var imgErroPop: UIImageView!
    
    
    var arrVisitorType = ["Car","Bike"]
    
    let dropDown = DropDown()
    
    @IBOutlet weak var viewSucussPop: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtVehicleType.text = "Car"
        
        viewSucussPop.isHidden = true
        
        if txtVehicleType.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else if txtNumber.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else
        {
            btnSave.alpha = 1
            btnSave.isUserInteractionEnabled = true
        }
        
        txtVehicleType.delegate = self
        txtNumber.delegate = self
        
        self.txtVehicleType.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtNumber.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        
        
        txtVehicleType.label.text = "Vehicle Type"
        txtVehicleType.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtVehicleType.placeholder = "Vehicle Type"
        txtVehicleType.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtVehicleType.leadingAssistiveLabel.text = "This field is required"
        //        txtVehicleType.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtVehicleType.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtVehicleType.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtVehicleType.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtVehicleType.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVehicleType.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtVehicleType.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtVehicleType.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVehicleType.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtVehicleType.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtVehicleType.containerRadius = 15
        txtVehicleType.leadingEdgePaddingOverride = 25
        txtVehicleType.tintColor = UIColor.clear
        txtVehicleType.addTarget(self, action: #selector(clickedOpenDrop), for: .editingDidBegin)
        
        
        
        txtNumber.label.text = "Vehicle Number"
        txtNumber.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtNumber.placeholder = "Vehicle Number"
        txtNumber.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtNumber.leadingAssistiveLabel.text = "This field is required"
        //        txtNumber.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtNumber.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtNumber.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtNumber.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtNumber.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNumber.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtNumber.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtNumber.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNumber.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtNumber.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtNumber.containerRadius = 15
        txtNumber.leadingEdgePaddingOverride = 25
        txtNumber.keyboardType = .default
        
        
        setUp()
        setDropDown()
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtVehicleType.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else if txtNumber.text == ""
        {
            btnSave.alpha = 0.5
            btnSave.isUserInteractionEnabled = false
        }
        else
        {
            btnSave.alpha = 1
            btnSave.isUserInteractionEnabled = true
        }
        
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    func setDropDown() {
        
        dropDown.dataSource = arrVisitorType
        dropDown.anchorView = viewTqrget
        dropDown.direction = .any
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            txtVehicleType.text = item
            txtVehicleType.resignFirstResponder()
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTqrget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTqrget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    @objc func clickedOpenDrop()
    {
        dropDown.show()
    }
    @IBAction func clickedSelectDrop(_ sender: Any) {
        
        txtVehicleType.becomeFirstResponder()
        self.view.endEditing(true)
        dropDown.show()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSave(_ sender: Any) {
        self.view.endEditing(true)
        callAddVehicalAPI()
        
    }
    
    @IBAction func clickedCancelPop(_ sender: Any) {
        viewSucussPop.isHidden = true
    }
    
    
    // MARK: - API Call
    func callAddVehicalAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["number_plate": txtNumber.text ?? "","vehicle_type": txtVehicleType.text ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPOST(CREATE_VEHICLE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                let arrData = (response?["data"] as? NSArray)?[0] as? String
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            self.viewSucussPop.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewSucussPop.isHidden = true
                            }
                            
                            self.viewSucussPop.backgroundColor = GREEN_Light_COLOR
                            self.viewSucussPop.borderColor = GREEN_COLOR
                            self.lblErrorMsg.textColor = GREEN_COLOR
                            
                            
                            self.imgErroPop.image = UIImage(named: "ic_Success_Msg")
                            self.lblErrorMsg.text = "Saved Successfully..!"
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                        else
                        {
                            self.viewSucussPop.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewSucussPop.isHidden = true
                            }
                            
                            self.viewSucussPop.backgroundColor = RED_Light_COLOR
                            self.viewSucussPop.borderColor = RED_COLOR
                            self.lblErrorMsg.textColor = RED_COLOR
                            
                            
                            self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                            self.lblErrorMsg.text = arrData
                        }
                        
                    } else {
                        
                        self.viewSucussPop.isHidden = false
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewSucussPop.isHidden = true
                        }
                        
                        self.viewSucussPop.backgroundColor = RED_Light_COLOR
                        self.viewSucussPop.borderColor = RED_COLOR
                        self.lblErrorMsg.textColor = RED_COLOR
                        
                        
                        self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                        self.lblErrorMsg.text = arrData
                    }
                    
                } else  if statusCode == 201 {
                    
                    if status == 1
                    {
                        self.viewSucussPop.isHidden = false
                        self.viewSucussPop.backgroundColor = GREEN_Light_COLOR
                        
                        self.viewSucussPop.borderColor = GREEN_COLOR
                        self.lblErrorMsg.textColor = GREEN_COLOR
                        
                        
                        self.imgErroPop.image = UIImage(named: "ic_Success_Msg")
                        self.lblErrorMsg.text = "Saved Successfully..!"
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else
                    {
                        self.viewSucussPop.isHidden = false
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewSucussPop.isHidden = true
                        }
                        
                        self.viewSucussPop.backgroundColor = RED_Light_COLOR
                        self.viewSucussPop.borderColor = RED_COLOR
                        self.lblErrorMsg.textColor = RED_COLOR
                        
                        
                        self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                        self.lblErrorMsg.text = arrData
                    }
                    
                }
                else
                {
                    self.viewSucussPop.isHidden = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewSucussPop.isHidden = true
                    }
                    
                    self.viewSucussPop.backgroundColor = RED_Light_COLOR
                    self.viewSucussPop.borderColor = RED_COLOR
                    self.lblErrorMsg.textColor = RED_COLOR
                    
                    
                    self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                    self.lblErrorMsg.text = arrData
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
