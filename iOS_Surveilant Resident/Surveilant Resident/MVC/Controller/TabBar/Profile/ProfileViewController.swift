//
//  ProfileViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 18/08/21.
//

import UIKit
import MessageUI

class ProfileViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    @IBOutlet weak var viewFeedback: UIView!
    @IBOutlet weak var viewFeedbackPopup: UIView!
    @IBOutlet weak var viewLogoutpopUp: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblName.text = appDelagte.dicLoginUserDetails?.name
        lblEmail.text = appDelagte.dicLoginUserDetails?.email
        lblMobile.text = appDelagte.dicLoginUserDetails?.phone
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewFeedback.isUserInteractionEnabled = true
        viewFeedback.addGestureRecognizer(tap)
        
        viewFeedback.isHidden = true
        viewFeedbackPopup.isHidden = true
        viewLogoutpopUp.isHidden = true

        DispatchQueue.main.async {
            self.viewTop.layer.cornerRadius = 10
            self.viewTop.layer.masksToBounds = true
            self.viewTop.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [UIColor(red: 41/255, green: 52/255, blue: 208/255, alpha: 1).cgColor, UIColor(red: 74/255, green: 85/255, blue: 232/255, alpha: 1).cgColor], type: .axial)
        }
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        setView(view: viewFeedback, hidden: true)
        setView(view: viewFeedbackPopup, hidden: true)
        setView(view: viewLogoutpopUp, hidden: true)

    }
    
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Action MEthod
    
    @IBAction func clickedHome(_ sender: Any) {
        appDelagte.setUpHome()
    }
    
    @IBAction func clickedPayment(_ sender: Any) {
        appDelagte.setUpPayemnt()
    }
    
    @IBAction func clickedChat(_ sender: Any) {
        appDelagte.setUpComplaint()
    }
    
    @IBAction func clickedProfile(_ sender: Any) {
    }
    
    @IBAction func clickedMyFamialy(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyFamilyViewController") as! MyFamilyViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyFamilyViewController") as! MyFamilyViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func clickedMyVehicle(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddVechicleViewController") as! AddVechicleViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddVechicleViewController") as! AddVechicleViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func clickedSupport(_ sender: Any) {
        setView(view: viewFeedback, hidden: false)
        setView(view: viewFeedbackPopup, hidden: false)
    }
    
    @IBAction func clickedPassword(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                vc.isFromProfile = true
                appDelagte.isResetPasswordProfile = true
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                vc.isFromProfile = true
                appDelagte.isResetPasswordProfile = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        
        setView(view: viewFeedback, hidden: false)
        setView(view: viewLogoutpopUp, hidden: false)
    }
    
    @IBAction func clickedDismiss(_ sender: Any) {
        setView(view: viewFeedback, hidden: true)
        setView(view: viewFeedbackPopup, hidden: true)
        setView(view: viewLogoutpopUp, hidden: true)
    }
    
    @IBAction func clickedCancelLogoutPop(_ sender: Any) {
        setView(view: viewFeedback, hidden: true)
        setView(view: viewLogoutpopUp, hidden: true)
        setView(view: viewFeedbackPopup, hidden: true)
    }
    
    @IBAction func clickedLogoutPop(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "isUserLogin")
        UserDefaults.standard.synchronize()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                    let home = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                    let homeNavigation = UINavigationController(rootViewController: home)
                    homeNavigation.navigationBar.isHidden = true
                    appDelagte.window?.rootViewController = homeNavigation
                    appDelagte.window?.makeKeyAndVisible()
                default:
                    print("Unknown")
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let home = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                    let homeNavigation = UINavigationController(rootViewController: home)
                    homeNavigation.navigationBar.isHidden = true
                    appDelagte.window?.rootViewController = homeNavigation
                    appDelagte.window?.makeKeyAndVisible()
                }
            }
            
        }
       
    }
    
    @IBAction func clickedGotoFQ(_ sender: Any) {
    }
    
    @IBAction func clickedFeedbackEmail(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["surveillant@gmail.com"])
            mail.setMessageBody("", isHTML: true)

            present(mail, animated: true)
          } else {
            // show failure alert
          }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
      controller.dismiss(animated: true)
    }
}

// MARK: - Gradient

public enum CAGradientPoint {
    case topLeft
    case centerLeft
    case bottomLeft
    case topCenter
    case center
    case bottomCenter
    case topRight
    case centerRight
    case bottomRight
    var point: CGPoint {
        switch self {
        case .topLeft:
            return CGPoint(x: 0, y: 0)
        case .centerLeft:
            return CGPoint(x: 0, y: 0.5)
        case .bottomLeft:
            return CGPoint(x: 0, y: 1.0)
        case .topCenter:
            return CGPoint(x: 0.5, y: 0)
        case .center:
            return CGPoint(x: 0.5, y: 0.5)
        case .bottomCenter:
            return CGPoint(x: 0.5, y: 1.0)
        case .topRight:
            return CGPoint(x: 1.0, y: 0.0)
        case .centerRight:
            return CGPoint(x: 1.0, y: 0.5)
        case .bottomRight:
            return CGPoint(x: 1.0, y: 1.0)
        }
    }
}

extension CAGradientLayer {
    
    convenience init(start: CAGradientPoint, end: CAGradientPoint, colors: [CGColor], type: CAGradientLayerType) {
        self.init()
        self.frame.origin = CGPoint.zero
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors
        self.locations = (0..<colors.count).map(NSNumber.init)
        self.type = type
    }
}

extension UIView {
    
    func layerGradient(startPoint:CAGradientPoint, endPoint:CAGradientPoint ,colorArray:[CGColor], type:CAGradientLayerType ) {
        let gradient = CAGradientLayer(start: .topLeft, end: .topRight, colors: colorArray, type: type)
        gradient.frame.size = self.frame.size
        self.layer.insertSublayer(gradient, at: 0)
    }
}

