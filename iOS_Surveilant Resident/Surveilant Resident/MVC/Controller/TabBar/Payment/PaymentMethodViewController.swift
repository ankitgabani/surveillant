//
//  PaymentMethodViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 25/08/21.
//

import UIKit
import Razorpay

class PaymentMethodViewController: UIViewController,RazorpayPaymentCompletionProtocolWithData {
     
  
    @IBOutlet weak var lblTrms: UILabel!
    @IBOutlet weak var imgCheckUncheck: UIImageView!
    @IBOutlet weak var btnPayNow: UIButton!
    
    var objPayemnt: GetPaymentDataData?
    
    var razorpay: RazorpayCheckout!

    var isSelected = false
    let selectableString1 = "terms and Conditions"

    override func viewDidLoad() {
        super.viewDidLoad()
        

        razorpay = RazorpayCheckout.initWithKey(ROZER_API_KEY, andDelegateWithData: self)
        
        let attributedString1 = NSMutableAttributedString(string: "I have read and agree to the website", attributes: [
            .font: UIFont(name: "Sk-Modernist-Regular", size: 13.0)!,
            .foregroundColor: GRAY_COLOR
        ])
        
        let attributedString2 = NSMutableAttributedString(string: " terms and Conditions", attributes: [
            .font: UIFont(name: "Sk-Modernist-Regular", size: 13.0)!,
            .foregroundColor: PRIMARY_STATUSBAR_COLOR
        ])
                
        let priceNSMutableAttributedString = NSMutableAttributedString()
        priceNSMutableAttributedString.append(attributedString1)
        priceNSMutableAttributedString.append(attributedString2)
        
        lblTrms.attributedText = priceNSMutableAttributedString
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(labelTapped))
        lblTrms.addGestureRecognizer(tapGesture)
        lblTrms.isUserInteractionEnabled = true

        
        
        imgCheckUncheck.image = imgCheckUncheck.image?.withRenderingMode(.alwaysTemplate)
        imgCheckUncheck.tintColor = GRAY_COLOR
        
        btnPayNow.backgroundColor = UIColor(red: 41/255, green: 52/255, blue: 208/255, alpha: 0.46)
        btnPayNow.isUserInteractionEnabled = false
        
        setUp()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func labelTapped(gesture: UITapGestureRecognizer) {
            
            if gesture.didTapAttributedString(selectableString1, in: lblTrms) {
                
                print("\(selectableString1) tapped")
                
                if let url = URL(string: "https://razorpay.com/sample-application/") {
                    UIApplication.shared.open(url)
                }
                
                
            } else {
                
                print("Text tapped")
            }
        }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    internal func showPaymentForm(){
        
        let totalAmount = Double((objPayemnt?.amount)!)
        
        let finalAmount = totalAmount * 100.0
        
        let options: [String:Any] = [
            "amount": "\(finalAmount)", //This is in currency subunits. 100 = 100 paise= INR 1.
            "currency": "INR",//We support more that 92 international currencies.
            "description": "Surveilant Resident",
           "order_id": "\(objPayemnt?.orderId ?? "")",
            "prefill": [
                "contact": "+91\(appDelagte.dicLoginUserDetails?.phone ?? "")",
                "email": "\(appDelagte.dicLoginUserDetails?.email ?? "")"
            ],
            "theme": [
                "color": "#3A3BD0"
            ]
        ]
        razorpay.open(options)
    }
    
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", code, str)
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentErrorViewController") as! PaymentErrorViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentErrorViewController") as! PaymentErrorViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", payment_id)
        
        let razorpay_payment_id = response?["razorpay_payment_id"] as? String
        let razorpay_order_id = response?["razorpay_order_id"] as? String
        let razorpay_signature = response?["razorpay_signature"] as? String

        
        callCreatePaymentAPI(payment_unit_id: payment_id, str_order_id: razorpay_order_id!, str_signature: razorpay_signature!, str_payment_id: razorpay_payment_id ?? "")
    }
    
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    @IBAction func clickedCheck(_ sender: Any) {
        
        if isSelected == true
        {
            imgCheckUncheck.image = imgCheckUncheck.image?.withRenderingMode(.alwaysTemplate)
            imgCheckUncheck.tintColor = GRAY_COLOR
            
            isSelected = false
            imgCheckUncheck.image = UIImage(named: "ic_uncheckedBox")
            btnPayNow.backgroundColor = UIColor(red: 41/255, green: 52/255, blue: 208/255, alpha: 0.46)
            btnPayNow.isUserInteractionEnabled = false
            
        }
        else
        {
            imgCheckUncheck.image = imgCheckUncheck.image?.withRenderingMode(.alwaysTemplate)
            imgCheckUncheck.tintColor = PRIMARY_STATUSBAR_COLOR
            
            isSelected = true
            imgCheckUncheck.image = UIImage(named: "ic_checkedBox")
            
            btnPayNow.backgroundColor = UIColor(red: 41/255, green: 52/255, blue: 208/255, alpha: 1)
            btnPayNow.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedPayNow(_ sender: Any) {
        
        showPaymentForm()
        
//        if UIDevice().userInterfaceIdiom == .phone {
//            switch UIScreen.main.nativeBounds.height {
//            case 1136:
//                print("iPhone 5 or 5S or 5C")
//                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
//                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentStautsViewController") as! PaymentStautsViewController
//                self.navigationController?.pushViewController(vc, animated: true)
//            default:
//                print("Unknown")
//                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentStautsViewController") as! PaymentStautsViewController
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
    }
    
    func callCreatePaymentAPI(payment_unit_id: String,str_order_id: String,str_signature: String,str_payment_id: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let dicData = NSMutableDictionary()
        
        dicData.setValue(str_order_id, forKey: "order_id")
        dicData.setValue(str_payment_id, forKey: "payment_id")
        dicData.setValue(str_signature, forKey: "signature")
        
        let param = ["payment": dicData]
        
        print(param)

        let _url = "v1/residents/payment_units/\(objPayemnt?.id ?? "")/razorpay_payment"
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPOST(_url, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {

                            if UIDevice().userInterfaceIdiom == .phone {
                                switch UIScreen.main.nativeBounds.height {
                                case 1136:
                                    print("iPhone 5 or 5S or 5C")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentStautsViewController") as! PaymentStautsViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
                                default:
                                    print("Unknown")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentStautsViewController") as! PaymentStautsViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
extension NSMutableAttributedString {

   public func setAsLink(textToFind:String, linkURL:String) -> Bool {

    let foundRange = self.mutableString.range(of: textToFind)
       if foundRange.location != NSNotFound {
        self.addAttribute(NSAttributedString.Key.link, value: linkURL, range: foundRange)
           return true
       }
       return false
   }
}
extension UITapGestureRecognizer {
    
    func didTapAttributedString(_ string: String, in label: UILabel) -> Bool {
        
        guard let text = label.text else {
            
            return false
        }
        
        let range = (text as NSString).range(of: string)
        return self.didTapAttributedText(label: label, inRange: range)
    }
    
    private func didTapAttributedText(label: UILabel, inRange targetRange: NSRange) -> Bool {
        
        guard let attributedText = label.attributedText else {
            
            assertionFailure("attributedText must be set")
            return false
        }
        
        let textContainer = createTextContainer(for: label)
        
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(textContainer)
        
        let textStorage = NSTextStorage(attributedString: attributedText)
        if let font = label.font {
            
            textStorage.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(0, attributedText.length))
        }
        textStorage.addLayoutManager(layoutManager)
        
        let locationOfTouchInLabel = location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let alignmentOffset = aligmentOffset(for: label)
        
        let xOffset = ((label.bounds.size.width - textBoundingBox.size.width) * alignmentOffset) - textBoundingBox.origin.x
        let yOffset = ((label.bounds.size.height - textBoundingBox.size.height) * alignmentOffset) - textBoundingBox.origin.y
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - xOffset, y: locationOfTouchInLabel.y - yOffset)
        
        let characterTapped = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        let lineTapped = Int(ceil(locationOfTouchInLabel.y / label.font.lineHeight)) - 1
        let rightMostPointInLineTapped = CGPoint(x: label.bounds.size.width, y: label.font.lineHeight * CGFloat(lineTapped))
        let charsInLineTapped = layoutManager.characterIndex(for: rightMostPointInLineTapped, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return characterTapped < charsInLineTapped ? targetRange.contains(characterTapped) : false
    }
    
    private func createTextContainer(for label: UILabel) -> NSTextContainer {
        
        let textContainer = NSTextContainer(size: label.bounds.size)
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        return textContainer
    }
    
    private func aligmentOffset(for label: UILabel) -> CGFloat {
        
        switch label.textAlignment {
            
        case .left, .natural, .justified:
            
            return 0.0
        case .center:
            
            return 0.5
        case .right:
            
            return 1.0
            
            @unknown default:
            
            return 0.0
        }
    }
}
