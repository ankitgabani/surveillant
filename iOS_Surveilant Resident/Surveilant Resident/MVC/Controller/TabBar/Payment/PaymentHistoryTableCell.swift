//
//  PaymentHistoryTableCell.swift
//  Surveilant Resident
//
//  Created by Gabani King on 25/08/21.
//

import UIKit

class PaymentHistoryTableCell: UITableViewCell {

    @IBOutlet weak var imgStatus: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
