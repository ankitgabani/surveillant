//
//  PaymentViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 25/08/21.
//

import UIKit

class PaymentViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var TBLVIEW: UITableView!
    @IBOutlet weak var collectionViewBill: UICollectionView!
    @IBOutlet weak var imgNoData: UIImageView!
    
    @IBOutlet weak var imgEmtyPayment: UIImageView!
    var arrGetPaymentList: [GetPaymentDataData] = [GetPaymentDataData]()
    var arrAllPaymentList: [AllPaymentDataData] = [AllPaymentDataData]()

    
    var flowLayoutTrend: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 320, height: 170)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 22, bottom: 0, right: 22)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 18
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        self.imgEmtyPayment.isHidden = true
        self.imgNoData.isHidden = true
         
        
        callGetPaymentAPI()
        callAllPaymentAPI()
        TBLVIEW.delegate = self
        TBLVIEW.dataSource = self
        
        collectionViewBill.delegate = self
        collectionViewBill.dataSource = self
        self.collectionViewBill.collectionViewLayout = flowLayoutTrend
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrGetPaymentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewBill.dequeueReusableCell(withReuseIdentifier: "PaymentBillCollectionCell", for: indexPath) as! PaymentBillCollectionCell
        
        let dicData = arrGetPaymentList[indexPath.row]

        let title = dicData.payment.title ?? ""
        let amount = dicData.due_amount ?? 0.0

        let endDate = dicData.payment.endDate ?? ""

        cell.lblTutle.text = title
        cell.lblAmount.text = "₹ \(amount)"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: endDate)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "E, h:mm a"
        let dayInWeek = dateFormatter2.string(from: date!)
        cell.lblDat.text = dayInWeek

        cell.btnPay.tag = indexPath.row
        cell.btnPay.addTarget(self, action: #selector(clickedPaynow(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickedPaynow(sender: UIButton)
    {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
                vc.objPayemnt = arrGetPaymentList[sender.tag]
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
                vc.objPayemnt = arrGetPaymentList[sender.tag]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAllPaymentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TBLVIEW.dequeueReusableCell(withIdentifier: "PaymentHistoryTableCell") as! PaymentHistoryTableCell
        
        let dicData = arrAllPaymentList[indexPath.row]
        
        let status = dicData.status ?? ""
        let title = dicData.payment.title ?? ""

        let created_at = dicData.createdAt ?? ""
        
        cell.lblTitle.text = title
        
        if status == "partially_paid"
        {
            let amount = dicData.paid_amount ?? 0.0
            cell.lblPrice.text = "\(amount)"

        }
        else
        {
            let amount = dicData.paid_amount ?? 0.0
            cell.lblPrice.text = "\(amount)"

        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: created_at)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MMM dd"
        let dayInWeek = dateFormatter2.string(from: date!)
        cell.lblDate.text = dayInWeek
        
        if status == "partially_paid" || status == "paid"
        {
            cell.imgStatus.image = UIImage(named: "ic_success")
            cell.lblStatus.text = status
            cell.lblStatus.textColor = GREEN_COLOR
        }
        else
        {
            cell.imgStatus.image = UIImage(named: "ic_fails")
            cell.lblStatus.text = status
            cell.lblStatus.textColor = RED_COLOR
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    
    //MARK:- Action MEthod
    
    @IBAction func clickedHome(_ sender: Any) {
        appDelagte.setUpHome()
    }
    
    @IBAction func clickedPayment(_ sender: Any) {
    }
    
    @IBAction func clickedChat(_ sender: Any) {
        appDelagte.setUpComplaint()
    }
    
    @IBAction func clickedProfile(_ sender: Any) {
        appDelagte.setUpProfile()
    }
    
    
    // MARK: - API Call
    func callGetPaymentAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(GET_PAYMENT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrGetPaymentList.removeAll()
                            for obj in arrData! {
                                let dicData = GetPaymentDataData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrGetPaymentList.append(dicData)
                            }
                            
                            if self.arrGetPaymentList.count == 0
                            {
                                self.imgEmtyPayment.isHidden = false
                            }
                            else
                            {
                                self.imgEmtyPayment.isHidden = true
                            }
                            
                            
                            self.collectionViewBill.reloadData()
                          
                        } else {
                            if self.arrGetPaymentList.count == 0
                            {
                                self.imgEmtyPayment.isHidden = false
                            }
                            else
                            {
                                self.imgEmtyPayment.isHidden = true
                            }
                            
                            self.arrGetPaymentList.removeAll()
                            self.collectionViewBill.reloadData()
                        }
                        
                    } else {
                        if self.arrGetPaymentList.count == 0
                        {
                            self.imgEmtyPayment.isHidden = false
                        }
                        else
                        {
                            self.imgEmtyPayment.isHidden = true
                        }
                        
                        self.arrGetPaymentList.removeAll()
                        self.collectionViewBill.reloadData()
                    }
                    
                } else {
                    if self.arrGetPaymentList.count == 0
                    {
                        self.imgEmtyPayment.isHidden = false
                    }
                    else
                    {
                        self.imgEmtyPayment.isHidden = true
                    }
                    
                    self.arrGetPaymentList.removeAll()
                    self.collectionViewBill.reloadData()
                }
                
            } else {
                if self.arrGetPaymentList.count == 0
                {
                    self.imgEmtyPayment.isHidden = false
                }
                else
                {
                    self.imgEmtyPayment.isHidden = true
                }
                
                self.arrGetPaymentList.removeAll()
                self.collectionViewBill.reloadData()
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callAllPaymentAPI() {
        
     //   APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(ALL_PAYMENT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrAllPaymentList.removeAll()
                            for obj in arrData! {
                                let dicData = AllPaymentDataData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrAllPaymentList.append(dicData)
                            }
                            
                            if self.arrAllPaymentList.count == 0
                            {
                                self.imgNoData.isHidden = false
                            }
                            else
                            {
                                self.imgNoData.isHidden = true
                            }
                            
                            self.TBLVIEW.reloadData()
                          
                        } else {
                            if self.arrAllPaymentList.count == 0
                            {
                                self.imgNoData.isHidden = false
                            }
                            else
                            {
                                self.imgNoData.isHidden = true
                            }
                            self.arrAllPaymentList.removeAll()
                            self.TBLVIEW.reloadData()
                        }
                        
                    } else {
                        if self.arrAllPaymentList.count == 0
                        {
                            self.imgNoData.isHidden = false
                        }
                        else
                        {
                            self.imgNoData.isHidden = true
                        }
                        self.arrAllPaymentList.removeAll()
                        self.TBLVIEW.reloadData()
                    }
                    
                } else {
                    if self.arrAllPaymentList.count == 0
                    {
                        self.imgNoData.isHidden = false
                    }
                    else
                    {
                        self.imgNoData.isHidden = true
                    }
                    self.arrAllPaymentList.removeAll()
                    self.TBLVIEW.reloadData()
                }
                
            } else {
                if self.arrAllPaymentList.count == 0
                {
                    self.imgNoData.isHidden = false
                }
                else
                {
                    self.imgNoData.isHidden = true
                }
                self.arrAllPaymentList.removeAll()
                self.TBLVIEW.reloadData()
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
