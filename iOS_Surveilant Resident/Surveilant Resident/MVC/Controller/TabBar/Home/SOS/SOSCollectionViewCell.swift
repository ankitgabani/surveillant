//
//  SOSCollectionViewCell.swift
//  Surveilant Resident
//
//  Created by Gabani King on 16/08/21.
//

import UIKit

class SOSCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgSOS: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var bgView: UIView!

    
}
