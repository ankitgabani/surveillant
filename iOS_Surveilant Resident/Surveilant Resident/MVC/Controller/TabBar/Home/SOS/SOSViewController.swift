//
//  SOSViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 16/08/21.
//

import UIKit

class SOSViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var collectionview: UICollectionView!
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 41.0,
                                     bottom: 0.0,
                                     right: 51.0)
    let itemsPerRow: CGFloat = 2
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
        _flowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 41.0, bottom: 0.0, right: 51.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 23
        return _flowLayout
    }
    
    var arrNAme = ["Fire Service","Police","Security","Ambulance"]
    var arrSOSImage = ["ic_FireService","ic_Police","ic_Security","ic_Ambulance"]
    
    
    var str_police_no = ""
    var str_security_no = ""
    var str_ambulance_no = ""
    var str_fire_service_no = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callSOSContactAPI()
        collectionview.delegate = self
        collectionview.dataSource = self
        
        self.collectionview.collectionViewLayout = flowLayout
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNAme.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SOSCollectionViewCell", for: indexPath) as! SOSCollectionViewCell
        
        cell.lblName.text = arrNAme[indexPath.row]
        cell.imgSOS.image = UIImage(named: arrSOSImage[indexPath.row])
        
        if indexPath.row == 0
        {
            cell.bgView.backgroundColor = UIColor(hexString: "FFF4D4")
        }
        else if indexPath.row == 1
        {
            cell.bgView.backgroundColor = UIColor(hexString: "DDF1FF")
        }
        else if indexPath.row == 2
        {
            cell.bgView.backgroundColor = UIColor(hexString: "F5C3D2")
        }
        else
        {
            cell.bgView.backgroundColor = UIColor(hexString: "FFE6D6")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            callNumber(phoneNumber: str_fire_service_no)
        }
        else if indexPath.row == 1
        {
            callNumber(phoneNumber: str_police_no)
        }
        else if indexPath.row == 2
        {
            callNumber(phoneNumber: str_security_no)
        }
        else
        {
            callNumber(phoneNumber: str_ambulance_no)
        }
    }
    
    private func callNumber(phoneNumber: String) {
        guard let url = URL(string: "telprompt://\(phoneNumber)"),
              UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    
    // MARK: - API Call
    func callSOSContactAPI() {
      
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(SOS_CONTACT_NO, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let dicData = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let police_no = dicData?.value(forKey: "police_no") as? String
                            let security_no = dicData?.value(forKey: "security_no") as? String
                            let ambulance_no = dicData?.value(forKey: "ambulance_no") as? String
                            let fire_service_no = dicData?.value(forKey: "fire_service_no") as? String

                            self.str_police_no = police_no ?? ""
                            self.str_security_no = security_no ?? ""
                            self.str_ambulance_no = ambulance_no ?? ""
                            self.str_fire_service_no = fire_service_no ?? ""
                            
                        } else {
                             
                        }
                    }
                  
                    
                } else {
                    
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
