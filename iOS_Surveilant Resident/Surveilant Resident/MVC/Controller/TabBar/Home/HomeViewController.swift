//
//  HomeViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 14/08/21.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblClount: UILabel!
    @IBOutlet weak var imgNpAnno: UIImageView!
    @IBOutlet weak var imgNoData: UIImageView!
    
    @IBOutlet weak var collectionViewSlider: UICollectionView!
    
    var arrResidentNoticeList: [ResidentNoticeDataData] = [ResidentNoticeDataData]()
    var arrResidentEventList: [SRInviteIndexData] = [SRInviteIndexData]()
    
    
    var flowLayoutTrend: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 303, height: 185)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 25)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 2
        return _flowLayout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.collectionViewSlider.isHidden = false
        self.imgNpAnno.isHidden = true
        
        self.tblView.isHidden = false
        self.imgNoData.isHidden = true
        
        callNotiCountAPI()
        callResidentNotice()
        callResidentEvents()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        lblName.text = "Welcome \(appDelagte.dicLoginUserDetails?.name ?? "")"
        
        tblView.delegate = self
        tblView.dataSource = self
        
        setUp()
        
        collectionViewSlider.delegate = self
        collectionViewSlider.dataSource = self
        self.collectionViewSlider.collectionViewLayout = flowLayoutTrend
        
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrResidentEventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        
        let dicData = arrResidentEventList[indexPath.row]
        
        let name = dicData.invitees[0].name ?? ""
        cell.lblTitle.text = name
        
        let phone = dicData.invitees[0].phone ?? ""
        cell.lblNumber.text = phone
        
        let status_ = dicData.invitees[0].status ?? ""
        cell.lblStatus.text = status_
        
        let updatedAt = dicData.createdAt
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let date = dateFormatter.date(from: updatedAt!)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MMMM dd, h:mm a"
        let dayInWeek = dateFormatter2.string(from: date!)
        
        cell.lblTime.text = dayInWeek
        
        if status_ == "created"
        {
            cell.viewStatus.backgroundColor = UIColor(red: 0/255, green: 188/255, blue: 254/255, alpha: 1)
        }
        else if status_ == "invited"
        {
            cell.viewStatus.backgroundColor = UIColor(red: 19/255, green: 183/255, blue: 65/255, alpha: 1)
        }
        else if status_ == "Upcoming"
        {
            cell.viewStatus.backgroundColor = UIColor(red: 19/255, green: 183/255, blue: 65/255, alpha: 1)
        }
        else if status_ == "Visited"
        {
            cell.viewStatus.backgroundColor = UIColor(red: 0/255, green: 188/255, blue: 254/255, alpha: 1)
        }
        
        let purpose = dicData.purpose ?? ""
        
        if purpose == "Visitor"
        {
            cell.imgEvent.image = UIImage(named: "ic_Visitor")
        }
        else if purpose == "Serviceengineer"
        {
            cell.imgEvent.image = UIImage(named: "ic_Serviceengineer")
        }
        else if purpose == "Taxi"
        {
            cell.imgEvent.image = UIImage(named: "gift")
        }
        else if purpose == "Delivery"
        {
            cell.imgEvent.image = UIImage(named: "ic_Taxi")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 108
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrResidentNoticeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewSlider.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        let dicData = arrResidentNoticeList[indexPath.row]
        
        cell.lblTitle.text = dicData.title ?? ""
        cell.lblMessage.text = dicData.message ?? ""
        
        let notice_type = dicData.noticeType ?? ""
        
        if notice_type == "payment"
        {
            cell.imgPayment.image = UIImage(named: "Group-4")
            cell.imgHieghtcont.constant = 100
            cell.imgWidthCont.constant = 180
        }
        else if notice_type == "maintenance"
        {
            cell.imgPayment.image = UIImage(named: "Group 3690")
            cell.imgHieghtcont.constant = 120
            cell.imgWidthCont.constant = 130
        }
        else
        {
            cell.imgPayment.image = UIImage(named: "Group 3689")
            cell.imgHieghtcont.constant = 100
            cell.imgWidthCont.constant = 180
        }
        
        let updatedAt = dicData.createdAt
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let date = dateFormatter.date(from: updatedAt!)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MMM"
        let dayInWeek = dateFormatter2.string(from: date!)
        cell.lblMonth.text = dayInWeek
        
        let dateFormatter3 = DateFormatter()
        dateFormatter3.dateFormat = "dd/yy"
        let strDate = dateFormatter3.string(from: date!)
        cell.lblDate.text = strDate
        
        return cell
    }
    
    //MARK:- Action MEthod
    
    @IBAction func clickedHome(_ sender: Any) {
    }
    
    @IBAction func clickedPayment(_ sender: Any) {
        appDelagte.setUpPayemnt()
    }
    
    @IBAction func clickedChat(_ sender: Any) {
        appDelagte.setUpComplaint()
    }
    
    @IBAction func clickedProfile(_ sender: Any) {
        appDelagte.setUpProfile()
    }
    
    
    // MARK: - API Call
    func callNotiCountAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(NOTIFICATION_COUNT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? Int
                            
                            self.lblClount.text = "\(arrData ?? 0)"
                            
                        }
                        
                    }
                    
                } else {
                    
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    // MARK: - API Call
    func callResidentNotice() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(RESIDENT_NOTICE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrResidentNoticeList.removeAll()
                            for obj in arrData! {
                                let dicData = ResidentNoticeDataData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrResidentNoticeList.append(dicData)
                            }
                            
                            if self.arrResidentNoticeList.count == 0
                            {
                                self.collectionViewSlider.isHidden = true
                                self.imgNpAnno.isHidden = false
                            }
                            else
                            {
                                self.collectionViewSlider.isHidden = false
                                self.imgNpAnno.isHidden = true
                            }
                            
                            self.collectionViewSlider.reloadData()
                            
                        } else {
                            if self.arrResidentNoticeList.count == 0
                            {
                                self.collectionViewSlider.isHidden = true
                                self.imgNpAnno.isHidden = false
                            }
                            else
                            {
                                self.collectionViewSlider.isHidden = false
                                self.imgNpAnno.isHidden = true
                            }
                        }
                        
                    } else {
                        if self.arrResidentNoticeList.count == 0
                        {
                            self.collectionViewSlider.isHidden = true
                            self.imgNpAnno.isHidden = false
                        }
                        else
                        {
                            self.collectionViewSlider.isHidden = false
                            self.imgNpAnno.isHidden = true
                        }
                        
                    }
                    
                } else {
                    if self.arrResidentNoticeList.count == 0
                    {
                        self.collectionViewSlider.isHidden = true
                        self.imgNpAnno.isHidden = false
                    }
                    else
                    {
                        self.collectionViewSlider.isHidden = false
                        self.imgNpAnno.isHidden = true
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callResidentEvents() {
        
        //APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(RESIDENTS_INVITE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrResidentEventList.removeAll()
                            for obj in arrData! {
                                let dicData = SRInviteIndexData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrResidentEventList.append(dicData)
                            }
                            
                            if self.arrResidentEventList.count == 0
                            {
                                self.tblView.isHidden = true
                                self.imgNoData.isHidden = false
                            }
                            else
                            {
                                self.tblView.isHidden = false
                                self.imgNoData.isHidden = true
                            }
                            
                            self.tblView.reloadData()
                            
                        } else {
                            if self.arrResidentEventList.count == 0
                            {
                                self.tblView.isHidden = true
                                self.imgNoData.isHidden = false
                            }
                            else
                            {
                                self.tblView.isHidden = false
                                self.imgNoData.isHidden = true
                            }
                        }
                        
                    } else {
                        if self.arrResidentEventList.count == 0
                        {
                            self.tblView.isHidden = true
                            self.imgNoData.isHidden = false
                        }
                        else
                        {
                            self.tblView.isHidden = false
                            self.imgNoData.isHidden = true
                        }
                        
                    }
                    
                } else {
                    if self.arrResidentEventList.count == 0
                    {
                        self.tblView.isHidden = true
                        self.imgNoData.isHidden = false
                    }
                    else
                    {
                        self.tblView.isHidden = false
                        self.imgNoData.isHidden = true
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
