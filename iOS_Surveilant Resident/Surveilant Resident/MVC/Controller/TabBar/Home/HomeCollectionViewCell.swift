//
//  HomeCollectionViewCell.swift
//  Surveilant Resident
//
//  Created by Mac MIni M1 on 14/08/21.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPayment: UIImageView!
    @IBOutlet weak var imgHieghtcont: NSLayoutConstraint!
    @IBOutlet weak var imgWidthCont: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
}
