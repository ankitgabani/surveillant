//
//  NotificationViewController.swift
//  Surveilant Resident
//
//  Created by Mac MIni M1 on 16/08/21.
//

import UIKit

class NotificationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNoNotification: UIView!
    
    var arrNoti = 5
    var arrNotiList: [NotificationDataData] = [NotificationDataData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewNoNotification.isHidden = true
        
        
        tblView.delegate = self
        tblView.dataSource = self
        callNotiAPI(showIndicator: true)
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotiList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        let dicData = arrNotiList[indexPath.row]
        
        
        cell.lblMsg.text = dicData.descriptionField
        cell.lblType.text = dicData.subject
        
        let status = dicData.status
        
        if status == "read"
        {
            cell.lblType.font = UIFont(name: "Sk-Modernist-Regular", size: 16)
            cell.lblMsg.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            
        }
        else
        {
            cell.lblType.font = UIFont(name: "Sk-Modernist-Bold", size: 16)
            cell.lblMsg.font = UIFont(name: "Sk-Modernist-Bold", size: 13)
            
        }
        
        if dicData.subject == "Payment"
        {
            cell.imgType.image = UIImage(named: "ic_PaymentZ")
        }
        else if dicData.subject  == "Support"
        {
            cell.imgType.image = UIImage(named: "ic_SupportZ")
        }
        else
        {
            cell.imgType.image = UIImage(named: "ic_ComplaintZ")
        }
        
        let updatedAt = dicData.createdAt
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let date = dateFormatter.date(from: updatedAt!)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "E,h:mm a"
        let dayInWeek = dateFormatter2.string(from: date!)
        
        cell.lblDate.text = dayInWeek
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dicData = arrNotiList[indexPath.row]
        
        let notfi_id = dicData.id
        callNotiStatusAPI(status: "read", notification_id: notfi_id ?? "")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let dicData = arrNotiList[indexPath.row]
        
        let notfi_id = dicData.id
        
        let closeAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("OK, marked as Closed")
            self.callNotiStatusAPI(status: "unread", notification_id: notfi_id ?? "")
            success(true)
        })
        closeAction.image = UIImage(named: "Rectangle 686")
        closeAction.image = UIImage(named: "ic_unread_Noti")
        closeAction.backgroundColor = UIColor(red: 2/255, green: 182/255, blue: 239/255, alpha: 1)
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let dicData = arrNotiList[indexPath.row]
        
        let notfi_id = dicData.id
        
        let modifyAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Update action ...")
            self.callNotiStatusAPI(status: "cleared", notification_id: notfi_id ?? "")
            success(true)
        })
        modifyAction.image = UIImage(named: "ic_delete_Noti")
        modifyAction.backgroundColor = UIColor(red: 234/255, green: 55/255, blue: 66/255, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [modifyAction])
    }
    
    
    @IBAction func clickedClearAll(_ sender: Any) {
        callNotiClearAllAPI()
    
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - API Call
    func callNotiAPI(showIndicator :Bool) {
        
        if showIndicator == true
        {
            APIClient.sharedInstance.showIndicator()
        }
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(NOTIFICATION_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrNotiList.removeAll()
                            for obj in arrData! {
                                let dicData = NotificationDataData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrNotiList.append(dicData)
                            }
                            
                            self.tblView.reloadData()
                            
                            if self.arrNotiList.count == 0
                            {
                                self.viewNoNotification.isHidden = false
                                self.tblView.isHidden = true
                            }
                            else
                            {
                                self.viewNoNotification.isHidden = true
                                self.tblView.isHidden = false
                            }
                            
                            
                        } else {
                            self.viewNoNotification.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                    } else {
                        
                        self.viewNoNotification.isHidden = false
                        self.tblView.isHidden = true
                    }
                    
                } else {
                    
                    self.viewNoNotification.isHidden = false
                    self.tblView.isHidden = true
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callNotiStatusAPI(status: String,notification_id: String) {
        
        let param = ["status": status]
        
        print(param)
        
        let url_ = "v1/residents/notifications/\(notification_id)/status"

        APIClient.sharedInstance.MakeAPICallWithAuthHeaderTokenPut(url_, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.callNotiAPI(showIndicator: false)
                            
                        } else {
                            self.viewNoNotification.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                    } else {
                        
                        self.viewNoNotification.isHidden = false
                        self.tblView.isHidden = true
                    }
                    
                } else {
                    
                    self.viewNoNotification.isHidden = false
                    self.tblView.isHidden = true
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callNotiClearAllAPI() {
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderTokenPut(NOTIFICATION_CLEAR, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.arrNotiList.removeAll()
                            self.viewNoNotification.isHidden = false
                            self.tblView.isHidden = true
                            
                        } else {
                            self.arrNotiList.removeAll()
                            self.viewNoNotification.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                    } else {
                        
                        self.arrNotiList.removeAll()
                        self.viewNoNotification.isHidden = false
                        self.tblView.isHidden = true
                    }
                    
                } else {
                    self.arrNotiList.removeAll()
                    self.viewNoNotification.isHidden = false
                    self.tblView.isHidden = true
                }
                
            } else {
                self.arrNotiList.removeAll()
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}

