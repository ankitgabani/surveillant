//
//  QuickInviteViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 16/08/21.
//

import UIKit
import MaterialComponents
import MDFInternationalization
import IQKeyboardManager

class QuickInviteViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate  {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtVisitorType: MDCOutlinedTextField!
    @IBOutlet weak var txtName: MDCOutlinedTextField!
    @IBOutlet weak var viewTargetType: UIView!
    @IBOutlet weak var txtMobile: MDCOutlinedTextField!
    @IBOutlet weak var txtVehicleNumber: MDCOutlinedTextField!
    @IBOutlet weak var txtDate: MDCOutlinedTextField!
    @IBOutlet weak var txtVisit: MDCOutlinedTextField!
    
    @IBOutlet weak var txtNote: MDCOutlinedTextArea!
    
    var arrVisitorType: [VisitorTypesDataData] = [VisitorTypesDataData]()
    var strVisitorType : String = ""
    let dropDown = DropDown()
    
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

    
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    fileprivate var today: Date = Date()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        callVisitorTypeAPI()
        
        if txtVisitorType.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtName.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtMobile.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtDate.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtVisit.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubmit.alpha = 1
            btnSubmit.isUserInteractionEnabled = true
        }
        
        
        txtVisitorType.delegate = self
        txtName.delegate = self
        txtVehicleNumber.delegate = self
        txtMobile.delegate = self
        txtDate.delegate = self
        txtVisit.delegate = self
        
        setUpTextField()
        setUp()
        self.txtVisitorType.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtName.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtVehicleNumber.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtMobile.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtDate.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtVisit.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtVisitorType.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtName.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtMobile.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtDate.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtVisit.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubmit.alpha = 1
            btnSubmit.isUserInteractionEnabled = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobile {
            
            if txtMobile.text?.count == 10
            {
                txtMobile.leadingAssistiveLabel.text = ""
                
                txtVisitorType.label.textColor = GRAY_COLOR
                
                txtMobile.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
                txtMobile.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
                txtMobile.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
                
                txtMobile.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
                txtMobile.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
                txtMobile.setFloatingLabelColor(GRAY_COLOR, for: .normal)
            }
            
            let newLength = (textField.text?.utf16.count)! + string.utf16.count - range.length
            if newLength <= 10 {
                let allowedCharacters = CharacterSet.decimalDigits
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            } else {
                return false
            }
        }
        else if textField == txtName
        {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")

            return (string == filtered)
        }
        else if textField == txtVehicleNumber
        {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")

            return (string == filtered)
        }
        else
        {
            return true
        }
        
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    
    func setUpTextField()
    {
        txtName.label.text = "Name*"
        txtName.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtName.placeholder = "Name*"
        txtName.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtName.leadingAssistiveLabel.text = "This field is required"
        //        txtName.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtName.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtName.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtName.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtName.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtName.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtName.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtName.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtName.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtName.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtName.containerRadius = 15
        txtName.leadingEdgePaddingOverride = 25
        txtName.keyboardType = .default
        txtName.addTarget(self, action: #selector(clickedShowKey), for: .editingDidBegin)
        
        
        txtVisitorType.delegate = self
        txtVisitorType.label.text = "Visitor Type*"
        txtVisitorType.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtVisitorType.placeholder = "Visitor Type*"
        txtVisitorType.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtVisitorType.leadingAssistiveLabel.text = "This field is required"
        //        txtVisitorType.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtVisitorType.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtVisitorType.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtVisitorType.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtVisitorType.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVisitorType.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtVisitorType.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtVisitorType.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVisitorType.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtVisitorType.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtVisitorType.containerRadius = 15
        txtVisitorType.leadingEdgePaddingOverride = 25
        txtVisitorType.tintColor = UIColor.clear
        txtVisitorType.addTarget(self, action: #selector(clickedOpenDrop), for: .editingDidBegin)
        
        
        txtMobile.label.text = "Mobile No*"
        txtMobile.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtMobile.placeholder = "Mobile No*"
        txtMobile.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtMobile.leadingAssistiveLabel.text = "This field is required"
        //        txtMobile.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtMobile.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtMobile.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtMobile.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtMobile.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtMobile.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtMobile.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtMobile.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtMobile.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtMobile.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtMobile.containerRadius = 15
        txtMobile.leadingEdgePaddingOverride = 25
        txtMobile.keyboardType = .phonePad
        txtMobile.addTarget(self, action: #selector(clickedShowKey), for: .editingDidBegin)
        
        
        
        txtVehicleNumber.label.text = "Vehicle Number"
        txtVehicleNumber.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtVehicleNumber.placeholder = "Vehicle Number"
        txtVehicleNumber.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtVehicleNumber.leadingAssistiveLabel.text = "This field is required"
        //        txtVehicleNumber.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtVehicleNumber.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtVehicleNumber.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtVehicleNumber.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtVehicleNumber.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVehicleNumber.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtVehicleNumber.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtVehicleNumber.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVehicleNumber.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtVehicleNumber.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtVehicleNumber.containerRadius = 15
        txtVehicleNumber.leadingEdgePaddingOverride = 25
        txtVehicleNumber.keyboardType = .default
        txtVehicleNumber.addTarget(self, action: #selector(clickedShowKey), for: .editingDidBegin)
        
        
        
        
        
        txtDate.label.text = "Date and Time*"
        txtDate.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtDate.placeholder = "Date and Time*"
        txtDate.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtDate.leadingAssistiveLabel.text = "This field is required"
        //        txtDate.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtDate.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtDate.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtDate.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtDate.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtDate.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtDate.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtDate.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtDate.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtDate.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtDate.containerRadius = 15
        txtDate.leadingEdgePaddingOverride = 25
        txtDate.tintColor = UIColor.clear
        
        txtDate.addTarget(self, action: #selector(clickedOpenDate), for: .editingDidBegin)
        
        
        txtVisit.label.text = "Purpose of Visit*"
        txtVisit.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtVisit.placeholder = "Purpose of Visit*"
        txtVisit.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtVisit.leadingAssistiveLabel.text = "This field is required"
        //        txtVisit.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtVisit.setLeadingAssistiveLabelColor(RED_COLOR, for: .editing)
        txtVisit.setLeadingAssistiveLabelColor(RED_COLOR, for: .disabled)
        txtVisit.setLeadingAssistiveLabelColor(RED_COLOR, for: .normal)
        
        txtVisit.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVisit.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtVisit.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtVisit.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtVisit.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtVisit.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtVisit.containerRadius = 15
        txtVisit.leadingEdgePaddingOverride = 25
        txtVisit.keyboardType = .default
        
        txtVisit.addTarget(self, action: #selector(clickedShowKey), for: .editingDidBegin)
        
        
        txtNote.textView.delegate = self
        txtNote.label.text = "Note"
        txtNote.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtNote.placeholder = "Note"
        txtNote.textView.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        //        txtNote.leadingAssistiveLabel.text = "This field is required"
        //        txtNote.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtNote.setLeadingAssistiveLabel(RED_COLOR, for: .editing)
        txtNote.setLeadingAssistiveLabel(RED_COLOR, for: .disabled)
        txtNote.setLeadingAssistiveLabel(RED_COLOR, for: .normal)
        
        txtNote.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNote.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtNote.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtNote.setFloatingLabel(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNote.setFloatingLabel(GRAY_COLOR, for: .disabled)
        txtNote.setFloatingLabel(GRAY_COLOR, for: .normal)
        
        txtNote.containerRadius = 15
        txtNote.leadingEdgePaddingOverride = 25
        txtNote.addTarget(self, action: #selector(clickedShowKey), for: .editingDidEnd)
        
        setDropDown()
    }
    
    @objc func clickedShowKey()
    {
        
    }
    
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        
        if txtVisitorType.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtName.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtMobile.text == ""
        {
            
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtDate.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtVisit.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubmit.alpha = 1
            btnSubmit.isUserInteractionEnabled = true
        }
    }
   
    
    @objc func clickedOpenDate()
    {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(Date())
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        selector.optionButtonShowCancel = true
        present(selector, animated: true, completion: nil)
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSubmit(_ sender: Any) {
        
        if txtMobile.text?.count != 10
        {
            txtMobile.leadingAssistiveLabel.text = "Mobile Number Length Must be 10"
            txtMobile.leadingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 12)
            
            txtMobile.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
            txtMobile.setOutlineColor(RED_COLOR, for: .disabled)
            txtMobile.setOutlineColor(RED_COLOR, for: .normal)
                        
            txtNote.setFloatingLabel(PRIMARY_STATUSBAR_COLOR, for: .editing)
            txtNote.setFloatingLabel(RED_COLOR, for: .disabled)
            txtNote.setFloatingLabel(RED_COLOR, for: .normal)
        }
        else
        {
            callCreateInviteAPI()
        }
        
    }
    
    func setDropDown() {
        
        let arrType = NSMutableArray()
        
        for obj in arrVisitorType
        {
            arrType.add(obj.name ?? "")
        }
        
        dropDown.dataSource = arrType as! [String]
        dropDown.anchorView = viewTargetType
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            txtVisitorType.text = item
            self.strVisitorType = arrVisitorType[index].aliasName
            txtVisitorType.resignFirstResponder()
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTargetType.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTargetType.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor(red: 134/255, green: 134/255, blue: 134/255, alpha: 1)
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.textFont = UIFont(name: "Sk-Modernist-Regular", size: 16)!
        dropDown.reloadAllComponents()
    }
    
    @objc func clickedOpenDrop()
    {
        
        dropDown.show()
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func clickedVisitorType(_ sender: Any) {
        
        txtVisitorType.becomeFirstResponder()
        self.view.endEditing(true)
        dropDown.show()
    }
    
    
    
    // MARK: - API Call
    func callVisitorTypeAPI() {
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(VISITOR_TYPES, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrVisitorType.removeAll()
                            for obj in arrData! {
                                let dicData = VisitorTypesDataData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrVisitorType.append(dicData)
                            }
                            
                            self.setDropDown()
                            
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                        
                    }
                    
                } else {
                    
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}


extension QuickInviteViewController: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool{
            let order = NSCalendar.current.compare(today, to: date, toGranularity: .day)
            if order == .orderedDescending{
                //Date selection will be disabled for past days
                return false
            } else {
                //Allows to select from today
                return true
            }
        }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
        txtDate.text = date.stringFromFormat("MMM dd, yyyy, hh:mm a")
        
        if txtVisitorType.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtName.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtMobile.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtDate.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else if txtVisit.text == ""
        {
            btnSubmit.alpha = 0.5
            btnSubmit.isUserInteractionEnabled = false
        }
        else
        {
            btnSubmit.alpha = 1
            btnSubmit.isUserInteractionEnabled = true
        }
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            
            txtDate.text = date.stringFromFormat("MMM dd, yyyy, hh:mm a")
            
            
            if txtVisitorType.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else if txtName.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else if txtMobile.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else if txtDate.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else if txtVisit.text == ""
            {
                btnSubmit.alpha = 0.5
                btnSubmit.isUserInteractionEnabled = false
            }
            else
            {
                btnSubmit.alpha = 1
                btnSubmit.isUserInteractionEnabled = true
            }
        }
        else {
            
            txtDate.text = "No Date Selected"
            
        }
        multipleDates = dates
    }
    
    func callCreateInviteAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let dicData = NSMutableDictionary()
        
        let arrData = NSMutableArray()
        
        let dicArrData = NSMutableDictionary()
        
        dicArrData.setValue(txtName.text!, forKey: "name")
        dicArrData.setValue(txtMobile.text!, forKey: "phone")
        dicArrData.setValue("91", forKey: "country_code")
        dicArrData.setValue(txtVehicleNumber.text ?? "", forKey: "vehicle_number")
        
        arrData.add(dicArrData)
        
        dicData.setValue(arrData, forKey: "invitees")
        dicData.setValue(self.strVisitorType ?? "", forKey: "purpose")
        dicData.setValue(txtDate.text ?? "", forKey: "visiting_time")
        dicData.setValue(txtNote.textView.text ?? "", forKey: "remarks")
        
        let param = ["invites": dicData]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPOST(CREATE_INVITE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            
                            if UIDevice().userInterfaceIdiom == .phone {
                                switch UIScreen.main.nativeBounds.height {
                                case 1136:
                                    print("iPhone 5 or 5S or 5C")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "InviteSentSuccessfullyVC") as! InviteSentSuccessfullyVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                default:
                                    print("Unknown")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "InviteSentSuccessfullyVC") as! InviteSentSuccessfullyVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    if UIDevice().userInterfaceIdiom == .phone {
                        switch UIScreen.main.nativeBounds.height {
                        case 1136:
                            print("iPhone 5 or 5S or 5C")
                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "InviteSentSuccessfullyVC") as! InviteSentSuccessfullyVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        default:
                            print("Unknown")
                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "InviteSentSuccessfullyVC") as! InviteSentSuccessfullyVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
