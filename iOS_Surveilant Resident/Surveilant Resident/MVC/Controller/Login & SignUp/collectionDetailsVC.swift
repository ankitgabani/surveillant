//
//  collectionDetailsVC.swift
//  Surveilant Resident
//
//  Created by Gabani King on 12/08/21.
//

import UIKit
import MaterialComponents
class collectionDetailsVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgNext: UIImageView!
    
    @IBOutlet weak var txtName: MDCOutlinedTextField!
    @IBOutlet weak var txtEmail: MDCOutlinedTextField!
    @IBOutlet weak var txtMobile: MDCOutlinedTextField!
    
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var imgPopUp: UIImageView!
    @IBOutlet weak var lblNamePopup: UILabel!
    @IBOutlet weak var viewValidationPopUpHieght: NSLayoutConstraint!
    
    var str_society_id = ""
    
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopUp.isHidden = true
        viewValidationPopUpHieght.constant = 0
        
        txtName.delegate = self
        txtEmail.delegate = self
        txtMobile.delegate = self
        
        if txtName.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtEmail.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtEmail.trailingAssistiveLabel.text == "Invalid mail :("
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtMobile.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        
        txtName.label.text = "Name*"
        txtName.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtName.placeholder = "Name*"
        txtName.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtName.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtName.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtName.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtName.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtName.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtName.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtName.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtName.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtName.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtName.containerRadius = 15
        txtName.leadingEdgePaddingOverride = 25
        txtName.keyboardType = .default

        
        txtEmail.label.text = "Email Id*"
        txtEmail.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtEmail.placeholder = "Email Id*"
        txtEmail.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtEmail.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtEmail.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmail.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtEmail.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtEmail.containerRadius = 15
        txtEmail.leadingEdgePaddingOverride = 25
        txtEmail.keyboardType = .emailAddress
        
        
        self.txtMobile.delegate = self
        
        txtMobile.label.text = "Mobile Number*"
        txtMobile.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtMobile.placeholder = "Mobile Number*"
        txtMobile.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtMobile.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtMobile.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtMobile.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtMobile.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtMobile.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtMobile.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtMobile.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtMobile.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtMobile.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        
        txtMobile.containerRadius = 15
        txtMobile.leadingEdgePaddingOverride = 25
        txtMobile.keyboardType = .phonePad
        
        setUp()
        self.txtName.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtMobile.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtEmail.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.btnNext.isUserInteractionEnabled = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobile {
            let newLength = (textField.text?.utf16.count)! + string.utf16.count - range.length
            if newLength <= 10 {
                let allowedCharacters = CharacterSet.decimalDigits
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            } else {
                return false
            }
        }
       else if textField == txtName
        {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")

            return (string == filtered)
        }
        else
        {
            return true
        }
        
    }
   
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtName == textfield
        {
            if txtName.text != ""
            {
                txtName.trailingAssistiveLabel.text = "Nice to meet you !"
                txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            else
            {
                txtName.trailingAssistiveLabel.text = ""
                txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            
            if txtMobile.text != ""
            {
                txtMobile.trailingAssistiveLabel.text = "One more step !"
                txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            else
            {
                txtMobile.trailingAssistiveLabel.text = ""
                txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            
            if txtEmail.text != ""
            {
                if !AppUtilites.isValidEmail(testStr: txtEmail.text ?? "")
                {
                    txtEmail.trailingAssistiveLabel.text = "Invalid mail :("
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .editing)
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .disabled)
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .normal)

                    txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
                }
                else
                {
                    txtEmail.trailingAssistiveLabel.text = "Valid mail :)"
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)

                    txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
                }
            }
            else
            {
                txtEmail.trailingAssistiveLabel.text = ""
                txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
        }
        else if txtEmail == textfield
        {
            if txtName.text != ""
            {
                txtName.trailingAssistiveLabel.text = "Nice to meet you !"
                txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            else
            {
                txtName.trailingAssistiveLabel.text = ""
                txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            
            if txtMobile.text != ""
            {
                txtMobile.trailingAssistiveLabel.text = "One more step !"
                txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            else
            {
                txtMobile.trailingAssistiveLabel.text = ""
                txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            
            if txtEmail.text != ""
            {
                if !AppUtilites.isValidEmail(testStr: txtEmail.text ?? "")
                {
                    txtEmail.trailingAssistiveLabel.text = "Invalid mail :("
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .editing)
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .disabled)
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .normal)
                    txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
                }
                else
                {
                    txtEmail.trailingAssistiveLabel.text = "Valid mail :)"
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)

                    txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
                }
            }
            else
            {
                txtEmail.trailingAssistiveLabel.text = ""
                txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
        }
        else if txtMobile == textfield
        {
            if txtEmail.text != ""
            {
                if !AppUtilites.isValidEmail(testStr: txtEmail.text ?? "")
                {
                    txtEmail.trailingAssistiveLabel.text = "Invalid mail :("
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .editing)
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .disabled)
                    txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .normal)
                    txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
                }
                else
                {
                    txtEmail.trailingAssistiveLabel.text = "Valid mail :)"
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
                    txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)

                    txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
                }
            }
            else
            {
                txtEmail.trailingAssistiveLabel.text = ""
                txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            
            if txtName.text != ""
            {
                txtName.trailingAssistiveLabel.text = "Nice to meet you !"
                txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            else
            {
                txtName.trailingAssistiveLabel.text = ""
                txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            
            if txtMobile.text != ""
            {
                txtMobile.trailingAssistiveLabel.text = "One more step !"
                txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
            else
            {
                txtMobile.trailingAssistiveLabel.text = ""
                txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            }
        }
        
        if txtName.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtEmail.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtEmail.trailingAssistiveLabel.text == "Invalid mail :("
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtMobile.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickedSignup(_ sender: Any) {
        
        if !AppUtilites.isValidEmail(testStr: txtEmail.text ?? "")
        {
            txtEmail.trailingAssistiveLabel.text = "Invalid mail :("
            txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .editing)
            txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .disabled)
            txtEmail.setTrailingAssistiveLabelColor(RED_COLOR, for: .normal)
            txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            
            txtName.trailingAssistiveLabel.text = "Nice to meet you !"
            txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            
            txtMobile.trailingAssistiveLabel.text = "One more step !"
            txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        }
        else
        {
            
            btnNext.isUserInteractionEnabled = false
            
            callCreateRegisterAPI()
            
            txtEmail.trailingAssistiveLabel.text = "Valid mail :)"
            txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
            txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
            txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)

            txtEmail.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            
            txtName.trailingAssistiveLabel.text = "Nice to meet you !"
            txtName.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            
            txtMobile.trailingAssistiveLabel.text = "One more step !"
            txtMobile.trailingAssistiveLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
            
            
        }
        
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clickedCancelValidPopUP(_ sender: Any) {
        viewPopUp.isHidden = true
        viewValidationPopUpHieght.constant = 0
    }
    
    func callCreateRegisterAPI() {
        
        //     APIClient.sharedInstance.showIndicator()
        
        let dicData = NSMutableDictionary()
        
        dicData.setValue(txtEmail.text!, forKey: "email")
        dicData.setValue(txtName.text!, forKey: "name")
        dicData.setValue(self.str_society_id, forKey: "society_id")
        dicData.setValue(txtMobile.text!, forKey: "phone")
        
        let param = ["resident": dicData]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPOST(REGISTER_RESIDENT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.viewPopUp.borderColor = GREEN_COLOR
                            self.viewPopUp.backgroundColor = GREEN_Light_COLOR
                            self.lblNamePopup.textColor = GREEN_COLOR
                            self.lblNamePopup.text = "Profile created successfully"
                            self.imgPopUp.image = UIImage(named: "ic_Success_Msg")
                            
                            let data_Str = response?["data"] as? String ?? ""
                            UserDefaults.standard.setValue(data_Str, forKey: "AuthorizationTOKEN")
                            UserDefaults.standard.synchronize()

                            
                            self.viewPopUp.isHidden = false
                            self.viewValidationPopUpHieght.constant = 44
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewPopUp.isHidden = true
                            }
                           
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        self.btnNext.isUserInteractionEnabled = true
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                        vc.isFromRegister = true
                                        vc.str_MobileNumer = self.txtMobile.text ?? ""
                                        vc.str_Email = self.txtEmail.text ?? ""
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        self.btnNext.isUserInteractionEnabled = true
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                        vc.isFromRegister = true
                                        vc.str_MobileNumer = self.txtMobile.text ?? ""
                                        vc.str_Email = self.txtEmail.text ?? ""
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            
                        } else {
                            
                            self.btnNext.isUserInteractionEnabled = true
                            
                            let arrData = response?["data"] as? NSArray
                            
                            let str_Msg = arrData?[0] as? String
                            
                            self.viewPopUp.isHidden = false
                            self.viewValidationPopUpHieght.constant = 44
                            
                            self.viewPopUp.borderColor = RED_COLOR
                            self.viewPopUp.backgroundColor = RED_Light_COLOR
                            self.lblNamePopup.textColor = RED_COLOR
                            self.lblNamePopup.text = str_Msg
                            self.imgPopUp.image = UIImage(named: "ic_Error_Msg")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewPopUp.isHidden = true
                            }
                        }
                        
                    }
                } else {
                    
                    self.btnNext.isUserInteractionEnabled = true
                    
                    let arrData = response?["data"] as? NSArray
                    
                    let str_Msg = arrData?[0] as? String
                    
                    if str_Msg == nil
                    {
                        self.lblNamePopup.text = "Internal Server Error"
                    }
                    else
                    {
                        self.lblNamePopup.text = str_Msg
                    }
                    
                    self.viewPopUp.isHidden = false
                    self.viewValidationPopUpHieght.constant = 44
                    
                    self.viewPopUp.borderColor = RED_COLOR
                    self.viewPopUp.backgroundColor = RED_Light_COLOR
                    self.lblNamePopup.textColor = RED_COLOR
                   
                    self.imgPopUp.image = UIImage(named: "ic_Error_Msg")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewPopUp.isHidden = true
                    }
                }
                
            } else {
                self.btnNext.isUserInteractionEnabled = false
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    func callSendOTPAPI() {
      
        let param = ["username": txtEmail.text ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(SEND_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
//                if statusCode == 200 || statusCode == 201
//                {
//
//                    if let responseUser = response {
                        
                      //  if status == 1 {
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        self.btnNext.isUserInteractionEnabled = true
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                        vc.str_MobileNumer = self.txtMobile.text ?? ""
                                        vc.str_Email = self.txtEmail.text ?? ""
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        self.btnNext.isUserInteractionEnabled = true
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                        vc.str_MobileNumer = self.txtMobile.text ?? ""
                                        vc.str_Email = self.txtEmail.text ?? ""
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            
//                        } else {
//
//                        }
                        
//                    } else {
//
//                    }
//
//                } else {
//
//                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
