//
//  CreatePasswordVC.swift
//  Surveilant Resident
//
//  Created by Gabani King on 13/08/21.
//

import UIKit
import MaterialComponents

class CreatePasswordVC: UIViewController,UITextFieldDelegate {
        
    @IBOutlet weak var txtNewPass: MDCOutlinedTextField!
    @IBOutlet weak var txtConirmPass: MDCOutlinedTextField!
    
    @IBOutlet weak var imgPassHowNew: UIImageView!
    @IBOutlet weak var imgPassHowConfirm: UIImageView!
    
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewPopUp: UIView!
    
    
    var isHidePassNew = true
    var isHidePassConfirm = true
    
    var resetField = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopUp.isHidden = true
        
        txtNewPass.isSecureTextEntry = true
        isHidePassNew = true
        imgPassHowNew.image = UIImage(named: "ic_HidePass")
        
        
        txtConirmPass.isSecureTextEntry = true
        isHidePassConfirm = true
        imgPassHowConfirm.image = UIImage(named: "ic_HidePass")
        
        
        txtNewPass.delegate = self
        txtConirmPass.delegate = self
        
        
        if txtNewPass.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtConirmPass.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        
        txtNewPass.label.text = "New Password"
        txtNewPass.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtNewPass.placeholder = "New Password"
        txtNewPass.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtNewPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNewPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtNewPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtNewPass.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNewPass.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtNewPass.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtNewPass.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNewPass.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtNewPass.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtNewPass.containerRadius = 15
        txtNewPass.leadingEdgePaddingOverride = 25
        txtNewPass.keyboardType = .default
        txtNewPass.isSecureTextEntry = true
        
        
        txtConirmPass.label.text = "Confirm Password"
        txtConirmPass.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtConirmPass.placeholder = "Confirm Password"
        txtConirmPass.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtConirmPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtConirmPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtConirmPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtConirmPass.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtConirmPass.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtConirmPass.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtConirmPass.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtConirmPass.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtConirmPass.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtConirmPass.containerRadius = 15
        txtConirmPass.leadingEdgePaddingOverride = 25
        txtConirmPass.keyboardType = .default
        txtConirmPass.isSecureTextEntry = true
        
        setUp()
        self.txtNewPass.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtConirmPass.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtNewPass.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtConirmPass.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        
        if resetField == true
        {
            callSetPasswordAPI()
        }
        else
        {
            callResetPasswordAPI()
        }
    }
    
    
    @IBAction func clickedCancle(_ sender: Any) {
        viewPopUp.isHidden = true
    }
    
    @IBAction func clickedNewPassShowHide(_ sender: Any)
    {
        if isHidePassNew == true
        {
            txtNewPass.isSecureTextEntry = false
            isHidePassNew = false
            imgPassHowNew.image = UIImage(named: "is_ShowPass")
        }
        else
        {
            txtNewPass.isSecureTextEntry = true
            isHidePassNew = true
            imgPassHowNew.image = UIImage(named: "ic_HidePass")
        }
    }
    
    @IBAction func clickedConfirmPassShowHide(_ sender: Any)
    {
        if isHidePassConfirm == true
        {
            txtConirmPass.isSecureTextEntry = false
            isHidePassConfirm = false
            imgPassHowConfirm.image = UIImage(named: "is_ShowPass")
        }
        else
        {
            txtConirmPass.isSecureTextEntry = true
            isHidePassConfirm = true
            imgPassHowConfirm.image = UIImage(named: "ic_HidePass")
            
        }
    }
    
    
    // MARK: - API Call
    
    func callSetPasswordAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["password": txtNewPass.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(SET_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.viewPopUp.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewPopUp.isHidden = true
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                appDelagte.setUpHome()
                            }
                            
                        } else {
                            
                           
                        }
                        
                    } else {
                      
                    }
                    
                } else {
                   
                  
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    func callResetPasswordAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["password": txtNewPass.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(RESET_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.viewPopUp.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewPopUp.isHidden = true
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                appDelagte.setUpHome()
                            }
                            
                        } else {
                            
                        }
                        
                    } else {
                      
                    }
                    
                } else {
                   
                  
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
