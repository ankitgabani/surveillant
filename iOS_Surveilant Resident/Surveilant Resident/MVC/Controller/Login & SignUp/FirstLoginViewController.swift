//
//  LoginViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 11/08/21.
//

import UIKit
import MaterialComponents
import Toast_Swift

class FirstLoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtPassword: MDCOutlinedTextField!
    @IBOutlet weak var txtEmail: MDCOutlinedTextField!
    
    @IBOutlet weak var imgShowHidePAss: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var viewErrorPop: UIView!
    
    var isHidePass = true
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewErrorPop.isHidden = true
//        txtEmail.text = "ravi@katomaran.com"
//        txtPassword.text = "12345678"
        
        txtPassword.isSecureTextEntry = true
        isHidePass = true
        imgShowHidePAss.image = UIImage(named: "ic_HidePass")
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        if txtEmail.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtPassword.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        
        
        txtEmail.label.text = "Email Id / Mobile Number"
        txtEmail.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtEmail.placeholder = "Email Id / Mobile Number"
        txtEmail.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtEmail.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtEmail.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmail.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtEmail.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtEmail.containerRadius = 15
        txtEmail.leadingEdgePaddingOverride = 25
        txtEmail.keyboardType = .emailAddress
        
        
        txtPassword.label.text = "Password"
        txtPassword.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtPassword.placeholder = "Password"
        txtPassword.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtPassword.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPassword.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtPassword.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtPassword.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPassword.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtPassword.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtPassword.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPassword.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtPassword.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtPassword.containerRadius = 15
        txtPassword.leadingEdgePaddingOverride = 25
        txtPassword.keyboardType = .default
        txtPassword.isSecureTextEntry = true
        
        
        setUp()
        
        self.txtEmail.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtPassword.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtEmail.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtPassword.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "LoginSelectionVC") as! LoginSelectionVC
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                appDelagte.window?.rootViewController = homeNavigation
                appDelagte.window?.makeKeyAndVisible()
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "LoginSelectionVC") as! LoginSelectionVC
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                appDelagte.window?.rootViewController = homeNavigation
                appDelagte.window?.makeKeyAndVisible()
            }
        }
        
    }
    
    @IBAction func clickedForPAss(_ sender: Any) {
        
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        
        self.view.endEditing(true)
        callLoginAPI()
    }
    
    @IBAction func clickedLoginOTP(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PhoneMailOTPViC") as! PhoneMailOTPViC
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "PhoneMailOTPViC") as! PhoneMailOTPViC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func clickedShowHide(_ sender: Any) {
        
        if isHidePass == true
        {
            txtPassword.isSecureTextEntry = false
            isHidePass = false
            imgShowHidePAss.image = UIImage(named: "is_ShowPass")
        }
        else
        {
            txtPassword.isSecureTextEntry = true
            isHidePass = true
            imgShowHidePAss.image = UIImage(named: "ic_HidePass")
        }
    }
    @IBAction func clickedHidePop(_ sender: Any) {
        viewErrorPop.isHidden = true
    }
    
    
    // MARK: - API Call
    func callLoginAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["username": txtEmail.text!,"password": txtPassword.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(LOGIN_RESIDENT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let dicUser_data = responseUser.value(forKey: "data") as? NSDictionary
                            
                            let dicData = UserLoginData(fromDictionary: dicUser_data!)
                            
                            appDelagte.saveCurrentUserData(dic: dicData)
                            appDelagte.dicLoginUserDetails = dicData
                            
                            UserDefaults.standard.set(true, forKey: "isUserLogin")
                            UserDefaults.standard.synchronize()
                                                        
                            if isFromRegister == true
                            {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CreatePasswordVC") as! CreatePasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CreatePasswordVC") as! CreatePasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            else if dicData.resetField == true
                            {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CreatePasswordVC") as! CreatePasswordVC
                                        vc.resetField = true
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CreatePasswordVC") as! CreatePasswordVC
                                        vc.resetField = true
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            else
                            {
                                appDelagte.setUpHome()
                            }
                            
                        } else {
                            
                            self.viewErrorPop.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewErrorPop.isHidden = true
                            }
                        }
                        
                    } else {
                        
                        self.viewErrorPop.isHidden = false
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewErrorPop.isHidden = true
                        }
                    }
                    
                } else {
                    
                    self.viewErrorPop.isHidden = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewErrorPop.isHidden = true
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
