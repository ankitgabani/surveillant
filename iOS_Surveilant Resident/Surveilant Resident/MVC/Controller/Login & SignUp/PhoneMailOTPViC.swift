//
//  PhoneMailOTPViC.swift
//  Surveilant Resident
//
//  Created by Gabani King on 13/08/21.
//

import UIKit
import MaterialComponents

class PhoneMailOTPViC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtPhoneEmail: MDCOutlinedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtPhoneEmail.delegate = self
        
        if txtPhoneEmail.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        
        txtPhoneEmail.label.text = "Email Id / Mobile Number"
        txtPhoneEmail.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtPhoneEmail.placeholder = "Email Id / Mobile Number"
        txtPhoneEmail.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtPhoneEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPhoneEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtPhoneEmail.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtPhoneEmail.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPhoneEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtPhoneEmail.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtPhoneEmail.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtPhoneEmail.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtPhoneEmail.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtPhoneEmail.containerRadius = 15
        txtPhoneEmail.leadingEdgePaddingOverride = 25
        txtPhoneEmail.keyboardType = .default
        
        setUp()
        self.txtPhoneEmail.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtPhoneEmail.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSendOTP(_ sender: Any) {
        
        callSendOTPAPI()
        
    }
    
    @IBAction func clickedLoginPAssword(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedREgister(_ sender: Any) {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ApartmentSelectionVC") as! ApartmentSelectionVC
                isFromRegister = true
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ApartmentSelectionVC") as! ApartmentSelectionVC
                isFromRegister = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    
    func callSendOTPAPI() {
      
        let param = ["username": txtPhoneEmail.text ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(SEND_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let data_Str = response?["data"] as? String ?? ""
                            UserDefaults.standard.setValue(data_Str, forKey: "AuthorizationTOKEN")
                            UserDefaults.standard.synchronize()
                            
                            
                            if UIDevice().userInterfaceIdiom == .phone {
                                switch UIScreen.main.nativeBounds.height {
                                case 1136:
                                    print("iPhone 5 or 5S or 5C")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                    vc.isFromOTPScreenOnly = true
                                    vc.str_Email = self.txtPhoneEmail.text ?? ""
                                    vc.str_MobileNumer = self.txtPhoneEmail.text ?? ""
                                    self.navigationController?.pushViewController(vc, animated: true)
                                default:
                                    print("Unknown")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                    vc.isFromOTPScreenOnly = true
                                    vc.str_Email = self.txtPhoneEmail.text ?? ""
                                    vc.str_MobileNumer = self.txtPhoneEmail.text ?? ""
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
