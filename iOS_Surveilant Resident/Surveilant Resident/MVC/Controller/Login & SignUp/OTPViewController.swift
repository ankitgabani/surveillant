//
//  OTPViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 12/08/21.
//

import UIKit
import OTPFieldView

class OTPViewController: UIViewController {
    
    @IBOutlet weak var otpView: OTPFieldView!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblTimeCount: UILabel!
    
    @IBOutlet weak var lblVerify: UILabel!
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnResnd: UIButton!
    
    @IBOutlet weak var lblDidntReceive: UILabel!
    
    @IBOutlet weak var viewInValidOTP: UIView!
    @IBOutlet weak var viewSendOTP: UIView!
    
    var str_MobileNumer = ""
    var str_Email = ""
    
    var enteredOtp = ""
    var isFromOTPScreenOnly = false
    var isFromForGotPass = false
    
    var count = 30
    var resendTimer = Timer()
    
    var isFromRegister = false
    var isClickOnResendBtn = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewSendOTP.isHidden = true
        self.viewInValidOTP.isHidden = true
        
        lblPhone.text = "Enter the OTP code sent to - \(str_MobileNumer)"
        
        btnResnd.isHidden = true
        lblTimeCount.isHidden = false
        resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
        if enteredOtp.count == 4
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        else
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        
        
        setupOtpView()
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    @objc func update() {
        if(count > 1) {
            count = count - 1
            //    print(count)
            if count < 10 {
                lblTimeCount.text = "00 : 0\(count)"
            }
            else
            {
                lblTimeCount.text = "00 : \(count)"
            }
        }
        else {
            resendTimer.invalidate()
            btnResnd.isHidden = false
            lblTimeCount.isHidden = true
            print("call your api")
            // if you want to reset the time make count = 60 and resendTime.fire()
        }
    }
    
    func setupOtpView(){
        self.otpView.fieldsCount = 4
        self.otpView.filledBackgroundColor = UIColor.clear
        self.otpView.defaultBackgroundColor = UIColor.clear
        self.otpView.cursorColor = PRIMARY_STATUSBAR_COLOR
        self.otpView.filledBorderColor = BORDER_NORMAL_COLOR
        self.otpView.defaultBorderColor = BORDER_NORMAL_COLOR
        self.otpView.displayType = .roundedCorner
        self.otpView.fieldBorderWidth = 1
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                self.otpView.fieldSize = 40
                self.otpView.cornerRadius = 15
                
            default:
                print("Unknown")
                self.otpView.fieldSize = 58
                self.otpView.cornerRadius = 15
            }
        }
        
        
        self.otpView.separatorSpace = 20
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.fieldFont = UIFont(name: "Sk-Modernist-Regular", size: 25.0)!
        self.otpView.initializeUI()
    }
    
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func clickedNext(_ sender: Any)
    {
        if self.isFromOTPScreenOnly == true
        {
            callVerifyRegistrationOTPVerifyAPI()
        }
        else if self.isFromForGotPass == true
        {
            callForgotPasswordOTPVerifyAPI()
        }
        else if self.isFromRegister == true
        {
            callRegisterOTPVerifyAPI()
        }
        else
        {
            callOTPVerifyAPI()
        }
    }
    
    
    @IBAction func clickedResend(_ sender: Any)
    {
        
        if isClickOnResendBtn == false
        {
            isClickOnResendBtn = true
            count = 30
            lblTimeCount.text = "00 : 30"
            resendTimer = Timer()
            btnResnd.isHidden = true
            lblTimeCount.isHidden = false
            resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
            
            if self.isFromForGotPass == true
            {
                callSendOTPAPI(type: FORGOT_PASSWORD)
            }
            else
            {
                callSendOTPAPI(type: SEND_OTP)
            }
        }
        else
        {
            lblDidntReceive.isHidden = true
            btnResnd.isHidden = true
            lblTimeCount.isHidden = true
        }
       
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedCancelqq(_ sender: Any) {
        viewSendOTP.isHidden = true
        viewInValidOTP.isHidden = true
    }
    
}
extension OTPViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        if hasEntered == true
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        else
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        self.enteredOtp = otpString
        
        if enteredOtp.count == 4
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        else
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
    }
    
    
    func callSendOTPAPI(type: String) {
        
        let param = ["username": str_Email]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(type, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            self.viewSendOTP.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewSendOTP.isHidden = true
                            }
                            
                        } else {
                            
                        }
                        
                    } else {
                        
                    }
                    
                } else {
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callRegisterOTPVerifyAPI() {
        
        let param = ["otp_token": enteredOtp,"username": str_Email]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(VERIFY_REGISTRATION, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            if UIDevice().userInterfaceIdiom == .phone {
                                switch UIScreen.main.nativeBounds.height {
                                case 1136:
                                    print("iPhone 5 or 5S or 5C")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                default:
                                    print("Unknown")
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            
                            
                        } else {
                            
                            self.viewInValidOTP.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewInValidOTP.isHidden = true
                            }
                            
                        }
                        
                    } else {
                        self.viewInValidOTP.isHidden = false
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewInValidOTP.isHidden = true
                        }
                    }
                    
                } else {
                    self.viewInValidOTP.isHidden = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewInValidOTP.isHidden = true
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    
    func callOTPVerifyAPI() {
        
        let param = ["otp_token": enteredOtp,"username": str_Email]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(OTP_VERIFY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            if self.isFromOTPScreenOnly == true
                            {
                                //Home
                                appDelagte.setUpHome()
                            }
                            else if self.isFromForGotPass == true
                            {
                                //Create New Pass (Forgot Pass)
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            else
                            {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            
                            
                        } else {
                            
                            self.viewInValidOTP.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewInValidOTP.isHidden = true
                            }
                            
                        }
                        
                    } else {
                        self.viewInValidOTP.isHidden = false
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewInValidOTP.isHidden = true
                        }
                    }
                    
                } else {
                    self.viewInValidOTP.isHidden = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewInValidOTP.isHidden = true
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callForgotPasswordOTPVerifyAPI() {
        
        let param = ["otp_token": enteredOtp,"username": str_Email]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(VALIDATE_FORGOT_PASSWORD_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            if self.isFromOTPScreenOnly == true
                            {
                                //Home
                                appDelagte.setUpHome()
                            }
                            else if self.isFromForGotPass == true
                            {
                                
                                let data_Str = response?["data"] as? String ?? ""
                                UserDefaults.standard.setValue(data_Str, forKey: "AuthorizationTOKEN")
                                UserDefaults.standard.synchronize()
                                
                                //Create New Pass (Forgot Pass)
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            else
                            {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            
                            
                        } else {
                            
                            self.viewInValidOTP.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewInValidOTP.isHidden = true
                            }
                            
                        }
                        
                    } else {
                        self.viewInValidOTP.isHidden = false
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewInValidOTP.isHidden = true
                        }
                    }
                    
                } else {
                    self.viewInValidOTP.isHidden = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewInValidOTP.isHidden = true
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func callVerifyRegistrationOTPVerifyAPI() {
        
        let param = ["otp_token": enteredOtp,"username": str_Email]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(OTP_VERIFY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 || statusCode == 201
                {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            if self.isFromOTPScreenOnly == true
                            {
                                //Home
                                
                                let dicUser_data = responseUser.value(forKey: "data") as? NSDictionary
                                
                                let dicData = UserLoginData(fromDictionary: dicUser_data!)
                                
                                appDelagte.saveCurrentUserData(dic: dicData)
                                appDelagte.dicLoginUserDetails = dicData
                                
                                UserDefaults.standard.set(true, forKey: "isUserLogin")
                                UserDefaults.standard.synchronize()
                                
                                appDelagte.setUpHome()
                                
                            }
                            else if self.isFromForGotPass == true
                            {
                                //Create New Pass (Forgot Pass)
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            else
                            {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SuccessScreenVC") as! SuccessScreenVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            
                            
                        } else {
                            
                            self.viewInValidOTP.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewInValidOTP.isHidden = true
                            }
                            
                        }
                        
                    } else {
                        self.viewInValidOTP.isHidden = false
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewInValidOTP.isHidden = true
                        }
                    }
                    
                } else {
                    self.viewInValidOTP.isHidden = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewInValidOTP.isHidden = true
                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
}
