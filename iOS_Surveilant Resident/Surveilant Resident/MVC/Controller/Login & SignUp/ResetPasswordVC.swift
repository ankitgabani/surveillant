//
//  ResetPasswordVC.swift
//  Surveilant Resident
//
//  Created by Gabani King on 14/08/21.
//

import UIKit
import MaterialComponents

class ResetPasswordVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var btnNext: UIButton!   
    @IBOutlet weak var txtNewPass: MDCOutlinedTextField!
    @IBOutlet weak var txtConfoimPAss: MDCOutlinedTextField!
    
    
    @IBOutlet weak var imgPassHowNew: UIImageView!
    @IBOutlet weak var imgPassHowConfirm: UIImageView!
    @IBOutlet weak var viewPopup: UIView!
    
    var isFromProfile = false
    
    var isHidePassNew = true
    var isHidePassConfirm = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopup.isHidden = true
        
        txtNewPass.isSecureTextEntry = true
        isHidePassNew = true
        imgPassHowNew.image = UIImage(named: "ic_HidePass")
        
        
        txtConfoimPAss.isSecureTextEntry = true
        isHidePassConfirm = true
        imgPassHowConfirm.image = UIImage(named: "ic_HidePass")
        
        
        if txtNewPass.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtConfoimPAss.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        
        txtNewPass.delegate = self
        txtConfoimPAss.delegate = self
        
        txtNewPass.label.text = "New Password"
        txtNewPass.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtNewPass.placeholder = "New Password"
        txtNewPass.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtNewPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNewPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtNewPass.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtNewPass.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNewPass.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtNewPass.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtNewPass.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtNewPass.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtNewPass.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtNewPass.containerRadius = 15
        txtNewPass.leadingEdgePaddingOverride = 25
        txtNewPass.keyboardType = .default
        txtNewPass.isSecureTextEntry = true
        
        
        txtConfoimPAss.label.text = "Confirm Password"
        txtConfoimPAss.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtConfoimPAss.placeholder = "Confirm Password"
        txtConfoimPAss.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtConfoimPAss.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtConfoimPAss.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtConfoimPAss.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtConfoimPAss.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtConfoimPAss.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtConfoimPAss.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtConfoimPAss.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtConfoimPAss.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtConfoimPAss.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtConfoimPAss.containerRadius = 15
        txtConfoimPAss.leadingEdgePaddingOverride = 25
        txtConfoimPAss.keyboardType = .default
        txtConfoimPAss.isSecureTextEntry = true
        
        
        
        setUp()
        self.txtNewPass.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        self.txtConfoimPAss.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtNewPass.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else if txtConfoimPAss.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
    }
    
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickddNext(_ sender: Any) {
        callResetPasswordAPI()
    }
    
    @IBAction func clickedBac(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func clickedNewPassShowHide(_ sender: Any)
    {
        if isHidePassNew == true
        {
            txtNewPass.isSecureTextEntry = false
            isHidePassNew = false
            imgPassHowNew.image = UIImage(named: "is_ShowPass")
        }
        else
        {
            txtNewPass.isSecureTextEntry = true
            isHidePassNew = true
            imgPassHowNew.image = UIImage(named: "ic_HidePass")
        }
    }
    
    @IBAction func clickedHidePop(_ sender: Any) {
        viewPopup.isHidden = true
    }
    
    
    @IBAction func clickedConfirmPassShowHide(_ sender: Any)
    {
        if isHidePassConfirm == true
        {
            txtConfoimPAss.isSecureTextEntry = false
            isHidePassConfirm = false
            imgPassHowConfirm.image = UIImage(named: "is_ShowPass")
        }
        else
        {
            txtConfoimPAss.isSecureTextEntry = true
            isHidePassConfirm = true
            imgPassHowConfirm.image = UIImage(named: "ic_HidePass")
            
        }
    }
    
    
    // MARK: - API Call
    func callResetPasswordAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["password": txtNewPass.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(SET_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {

                    if let responseUser = response {

                        if status == 1 {
                            
                            self.viewPopup.isHidden = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewPopup.isHidden = true
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                                
                                if self.isFromProfile == true
                                {
                                    appDelagte.isResetPasswordProfile = false
                                    appDelagte.setUpHome()
                                }
                                else
                                {
                                    if UIDevice().userInterfaceIdiom == .phone {
                                        switch UIScreen.main.nativeBounds.height {
                                        case 1136:
                                            print("iPhone 5 or 5S or 5C")
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                            let home = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                                            let homeNavigation = UINavigationController(rootViewController: home)
                                            homeNavigation.navigationBar.isHidden = true
                                                                                        
                                            appDelagte.window?.rootViewController = homeNavigation
                                            appDelagte.window?.makeKeyAndVisible()
                                        default:
                                            print("Unknown")
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let home = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                                            let homeNavigation = UINavigationController(rootViewController: home)
                                            homeNavigation.navigationBar.isHidden = true
                                            appDelagte.window?.rootViewController = homeNavigation
                                            appDelagte.window?.makeKeyAndVisible()
                                        }
                                    }
                                    
                                }
                               
                            }

                        } else {


                        }

                    } else {

                    }

                } else {


                }

            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
