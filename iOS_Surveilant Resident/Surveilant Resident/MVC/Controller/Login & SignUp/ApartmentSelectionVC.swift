//
//  ApartmentSelectionVC.swift
//  Surveilant Resident
//
//  Created by Gabani King on 11/08/21.
//

import UIKit
import MaterialComponents

class ApartmentSelectionVC: UIViewController,UITextFieldDelegate{
    
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var txtApartmentName: MDCOutlinedTextField!
    
    var arrSocity: [SRAllSocietiesData] = [SRAllSocietiesData]()
    var arrSocitySeaching: [SRAllSocietiesData] = [SRAllSocietiesData]()

    let dropDown = DropDown()
    
    var isSearching = false
    
    var str_society_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callGetAllSocityAPI()
        
        if txtApartmentName.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        
        txtApartmentName.delegate = self
        
        txtApartmentName.label.text = "Apartment Name"
        txtApartmentName.label.font = UIFont(name: "Sk-Modernist-Regular", size: 14)
        
        txtApartmentName.placeholder = "Select Your Apartment"
        txtApartmentName.font = UIFont(name: "Sk-Modernist-Regular", size: 14)
        
        txtApartmentName.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtApartmentName.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtApartmentName.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtApartmentName.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtApartmentName.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtApartmentName.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtApartmentName.containerRadius = 15
        txtApartmentName.leadingEdgePaddingOverride = 25
        
        txtApartmentName.leadingEdgePaddingOverride = 25
        
        self.txtApartmentName.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    func setDropDown() {
        
        let arrType = NSMutableArray()
        
        for obj in arrSocitySeaching
        {
            arrType.add(obj.name ?? "")
        }
        
        dropDown.dataSource = arrType as! [String]
        dropDown.anchorView = viewTarget
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            txtApartmentName.text = item
            txtApartmentName.resignFirstResponder()
            
            for object in arrSocitySeaching
            {
                if object.name == item
                {
                    self.str_society_id = object.id
                }
            }
            
            if txtApartmentName.text == ""
            {
                imgNext.alpha = 0.5
                btnNext.isUserInteractionEnabled = false
            }
            else
            {
                imgNext.alpha = 1
                btnNext.isUserInteractionEnabled = true
            }
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor(red: 134/255, green: 134/255, blue: 134/255, alpha: 1)
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.textFont = UIFont(name: "Sk-Modernist-Regular", size: 16)!
        dropDown.reloadAllComponents()
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
        
        if txtApartmentName.text == ""
        {
            self.isSearching = false

            dropDown.hide()
        }
        else
        {
            self.isSearching = true
            
            self.arrSocitySeaching.removeAll(keepingCapacity: false)
            
            for i in 0..<self.arrSocity.count {
                
                let listItem: SRAllSocietiesData = self.arrSocity[i]
                if listItem.name.lowercased().range(of: self.txtApartmentName.text!) != nil
                {
                    self.arrSocitySeaching.append(listItem)
                }
            }
            
            self.setDropDown()
            dropDown.show()
        }
       
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        isFromRegister = false
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedNext(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "collectionDetailsVC") as! collectionDetailsVC
                vc.str_society_id = self.str_society_id
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "collectionDetailsVC") as! collectionDetailsVC
                vc.str_society_id = self.str_society_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    
    // MARK: - API Call
    func callGetAllSocityAPI() {
        
      //  APIClient.sharedInstance.showIndicator()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGET(ALL_SOCIETY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let arrData = responseUser.value(forKey: "data") as? NSArray
                            self.arrSocity.removeAll()
                            for obj in arrData! {
                                let dicData = SRAllSocietiesData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrSocity.append(dicData)
                            }
                           
                        } else {
                            self.arrSocity.removeAll()
                        }
                        
                    } else {
                        self.arrSocity.removeAll()
                    }
                    
                } else {
                    self.arrSocity.removeAll()
                }
                
            } else {
                self.arrSocity.removeAll()
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
