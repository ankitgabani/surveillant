//
//  ForgotPassViewController.swift
//  Surveilant Resident
//
//  Created by Gabani King on 14/08/21.
//

import UIKit
import MaterialComponents

class ForgotPassViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtEmailPhone: MDCOutlinedTextField!
    
    @IBOutlet weak var viewErrorPop: UIView!
    @IBOutlet weak var lblErrorMsg: UILabel!
    @IBOutlet weak var imgErroPop: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewErrorPop.isHidden = true
        
        txtEmailPhone.delegate = self
        
        if txtEmailPhone.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
        
        txtEmailPhone.label.text = "Email Id / Mobile Number"
        txtEmailPhone.label.font = UIFont(name: "Sk-Modernist-Regular", size: 13)
        
        txtEmailPhone.placeholder = "Email Id / Mobile Number"
        txtEmailPhone.font = UIFont(name: "Sk-Modernist-Regular", size: 15)
        
        txtEmailPhone.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmailPhone.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .disabled)
        txtEmailPhone.setTrailingAssistiveLabelColor(PRIMARY_STATUSBAR_COLOR, for: .normal)
        
        txtEmailPhone.setOutlineColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmailPhone.setOutlineColor(BORDER_NORMAL_COLOR, for: .disabled)
        txtEmailPhone.setOutlineColor(BORDER_NORMAL_COLOR, for: .normal)
        
        txtEmailPhone.setFloatingLabelColor(PRIMARY_STATUSBAR_COLOR, for: .editing)
        txtEmailPhone.setFloatingLabelColor(GRAY_COLOR, for: .disabled)
        txtEmailPhone.setFloatingLabelColor(GRAY_COLOR, for: .normal)
        
        txtEmailPhone.containerRadius = 15
        txtEmailPhone.leadingEdgePaddingOverride = 25
        txtEmailPhone.keyboardType = .default
        
        setUp()
        self.txtEmailPhone.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if txtEmailPhone.text == ""
        {
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
        else
        {
            imgNext.alpha = 1
            btnNext.isUserInteractionEnabled = true
        }
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedSendOTP(_ sender: Any) {
        self.view.endEditing(true)
        callFotgotPasswordAPI()
        
    }
    
    @IBAction func clickedHidePop(_ sender: Any) {
        self.viewErrorPop.isHidden = true
    }
    
    
    // MARK: - API Call
    func callFotgotPasswordAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["username": txtEmailPhone.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPut(FORGOT_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let data_Message = response?["message"] as? String ?? ""
                let status = response?["status"] as? Int ?? 0
                
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200 {
                    
                    if let responseUser = response {
                        
                        if status == 1 {
                            
                            let dicUser_data = responseUser.value(forKey: "data") as? String
                            
                            let data_Str = response?["data"] as? String ?? ""
                            UserDefaults.standard.setValue(data_Str, forKey: "AuthorizationTOKEN")
                            UserDefaults.standard.synchronize()
                            
                            
                            self.viewErrorPop.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewErrorPop.isHidden = true
                            }
                            
                            self.viewErrorPop.backgroundColor = GREEN_Light_COLOR
                            self.viewErrorPop.borderColor = GREEN_COLOR
                            self.lblErrorMsg.textColor = GREEN_COLOR
                            
                            self.imgErroPop.image = UIImage(named: "ic_Success_Msg")
                            self.lblErrorMsg.text = data_Message
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                                if UIDevice().userInterfaceIdiom == .phone {
                                    switch UIScreen.main.nativeBounds.height {
                                    case 1136:
                                        print("iPhone 5 or 5S or 5C")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                        vc.str_Email = self.txtEmailPhone.text ?? ""
                                        vc.isFromForGotPass = true
                                        vc.str_MobileNumer = self.txtEmailPhone.text ?? ""
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    default:
                                        print("Unknown")
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                        vc.isFromForGotPass = true
                                        vc.str_Email = self.txtEmailPhone.text ?? ""
                                        vc.str_MobileNumer = self.txtEmailPhone.text ?? ""
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            
                            
                        } else {
                            
                            self.viewErrorPop.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewErrorPop.isHidden = true
                            }
                            
                            self.viewErrorPop.backgroundColor = RED_Light_COLOR
                            self.viewErrorPop.borderColor = RED_COLOR
                            self.lblErrorMsg.textColor = RED_COLOR
                            
                            self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                            self.lblErrorMsg.text = data_Message
                        }
                        
                    } else {
                        
                        self.viewErrorPop.isHidden = false
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewErrorPop.isHidden = true
                        }
                        
                        self.viewErrorPop.backgroundColor = RED_Light_COLOR
                        self.viewErrorPop.borderColor = RED_COLOR
                        self.lblErrorMsg.textColor = RED_COLOR
                        
                        
                        self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                        self.lblErrorMsg.text = data_Message
                    }
                    
                } else {
                    
                    self.viewErrorPop.isHidden = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.viewErrorPop.isHidden = true
                    }
                    
                    self.viewErrorPop.backgroundColor = RED_Light_COLOR
                    self.viewErrorPop.borderColor = RED_COLOR
                    self.lblErrorMsg.textColor = RED_COLOR
                    
                    
                    self.imgErroPop.image = UIImage(named: "ic_Error_Msg")
                    self.lblErrorMsg.text = data_Message
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
}
