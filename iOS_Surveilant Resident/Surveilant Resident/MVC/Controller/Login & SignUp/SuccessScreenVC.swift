//
//  SuccessScreenVC.swift
//  Surveilant Resident
//
//  Created by Gabani King on 12/08/21.
//

import UIKit

class SuccessScreenVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
        // Do any additional setup after loading the view.
    }
    @IBAction func clickedLogin(_ sender: Any) {
        if UIDevice().userInterfaceIdiom == .phone {
                            switch UIScreen.main.nativeBounds.height {
                            case 1136:
                                print("iPhone 5 or 5S or 5C")
                                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                            default:
                                print("Unknown")
                                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FirstLoginViewController") as! FirstLoginViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
    }
    
    func setUp()
    {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = PRIMARY_STATUSBAR_COLOR
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = PRIMARY_STATUSBAR_COLOR
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
