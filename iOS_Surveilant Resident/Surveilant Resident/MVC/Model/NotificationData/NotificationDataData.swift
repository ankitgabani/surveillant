//
//	NotificationDataData.swift
//
//	Create by mac on 22/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class NotificationDataData : NSObject, NSCoding{

	var createdAt : String!
	var descriptionField : String!
	var id : String!
	var notificationType : String!
	var status : String!
	var subject : String!
	var updatedAt : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		notificationType = dictionary["notification_type"] as? String == nil ? "" : dictionary["notification_type"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		subject = dictionary["subject"] as? String == nil ? "" : dictionary["subject"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if id != nil{
			dictionary["id"] = id
		}
		if notificationType != nil{
			dictionary["notification_type"] = notificationType
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subject != nil{
			dictionary["subject"] = subject
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         notificationType = aDecoder.decodeObject(forKey: "notification_type") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         subject = aDecoder.decodeObject(forKey: "subject") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if notificationType != nil{
			aCoder.encode(notificationType, forKey: "notification_type")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if subject != nil{
			aCoder.encode(subject, forKey: "subject")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}