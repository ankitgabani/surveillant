//
//	GetPaymentDataPayment.swift
//
//	Create by Mac M1 on 22/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetPaymentDataPayment : NSObject, NSCoding{

	var descriptionField : String!
	var endDate : String!
	var title : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		endDate = dictionary["end_date"] as? String == nil ? "" : dictionary["end_date"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if endDate != nil{
			dictionary["end_date"] = endDate
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         endDate = aDecoder.decodeObject(forKey: "end_date") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if endDate != nil{
			aCoder.encode(endDate, forKey: "end_date")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}