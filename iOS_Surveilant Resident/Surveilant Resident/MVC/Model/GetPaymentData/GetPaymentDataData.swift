//
//	GetPaymentDataData.swift
//
//	Create by Mac M1 on 22/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetPaymentDataData : NSObject, NSCoding{

	var amount : Float!
	var authorId : String!
	var authorName : String!
	var createdAt : String!
	var descriptionField : String!
	var id : String!
	var orderId : String!
	var paidDate : String!
	var payment : GetPaymentDataPayment!
	var status : String!
	var updatedAt : String!
    var paid_amount: Float!
    var due_amount: Float!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		amount = dictionary["total_amount"] as? Float == nil ? 0 : dictionary["total_amount"] as? Float
		authorId = dictionary["author_id"] as? String == nil ? "" : dictionary["author_id"] as? String
		authorName = dictionary["author_name"] as? String == nil ? "" : dictionary["author_name"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		orderId = dictionary["order_id"] as? String == nil ? "" : dictionary["order_id"] as? String
		paidDate = dictionary["paid_date"] as? String == nil ? "" : dictionary["paid_date"] as? String
		if let paymentData = dictionary["payment"] as? NSDictionary{
			payment = GetPaymentDataPayment(fromDictionary: paymentData)
		}
		else
		{
			payment = GetPaymentDataPayment(fromDictionary: NSDictionary.init())
		}
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
        due_amount = dictionary["due_amount"] as? Float == nil ? 0.0 : dictionary["due_amount"] as? Float

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if amount != nil{
			dictionary["total_amount"] = amount
		}
		if authorId != nil{
			dictionary["author_id"] = authorId
		}
		if authorName != nil{
			dictionary["author_name"] = authorName
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if id != nil{
			dictionary["id"] = id
		}
		if orderId != nil{
			dictionary["order_id"] = orderId
		}
		if paidDate != nil{
			dictionary["paid_date"] = paidDate
		}
		if payment != nil{
			dictionary["payment"] = payment.toDictionary()
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
        if due_amount != nil{
            dictionary["due_amount"] = due_amount
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        due_amount = aDecoder.decodeObject(forKey: "due_amount") as? Float

         amount = aDecoder.decodeObject(forKey: "total_amount") as? Float
         authorId = aDecoder.decodeObject(forKey: "author_id") as? String
         authorName = aDecoder.decodeObject(forKey: "author_name") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         orderId = aDecoder.decodeObject(forKey: "order_id") as? String
         paidDate = aDecoder.decodeObject(forKey: "paid_date") as? String
         payment = aDecoder.decodeObject(forKey: "payment") as? GetPaymentDataPayment
         status = aDecoder.decodeObject(forKey: "status") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if amount != nil{
			aCoder.encode(amount, forKey: "total_amount")
		}
		if authorId != nil{
			aCoder.encode(authorId, forKey: "author_id")
		}
		if authorName != nil{
			aCoder.encode(authorName, forKey: "author_name")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if orderId != nil{
			aCoder.encode(orderId, forKey: "order_id")
		}
		if paidDate != nil{
			aCoder.encode(paidDate, forKey: "paid_date")
		}
		if payment != nil{
			aCoder.encode(payment, forKey: "payment")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
        if due_amount != nil{
            aCoder.encode(due_amount, forKey: "due_amount")
        }

	}

}
