//
//	VehiclesListData.swift
//
//	Create by mac on 22/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class VehiclesListData : NSObject, NSCoding{

	var id : String!
	var numberPlate : String!
	var vehicleType : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		numberPlate = dictionary["number_plate"] as? String == nil ? "" : dictionary["number_plate"] as? String
		vehicleType = dictionary["vehicle_type"] as? String == nil ? "" : dictionary["vehicle_type"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["id"] = id
		}
		if numberPlate != nil{
			dictionary["number_plate"] = numberPlate
		}
		if vehicleType != nil{
			dictionary["vehicle_type"] = vehicleType
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         numberPlate = aDecoder.decodeObject(forKey: "number_plate") as? String
         vehicleType = aDecoder.decodeObject(forKey: "vehicle_type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if numberPlate != nil{
			aCoder.encode(numberPlate, forKey: "number_plate")
		}
		if vehicleType != nil{
			aCoder.encode(vehicleType, forKey: "vehicle_type")
		}

	}

}