//
//	SRListComplaintsData.swift
//
//	Create by mac on 24/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRListComplaintsData : NSObject, NSCoding{

	var id : String!
	var index : String!
	var score : String!
	var type : String!
	var complaintCode : String!
	var complaintType : SRListComplaintsComplaintType!
	var complaintTypeId : String!
	var createdAt : String!
	var descriptionField : String!
	var identityId : Int!
	var resident : SRListComplaintsResident!
	var residentUnitId : String!
	var resolvedAt : String!
	var resolvedBy : String!
	var sort : [Int]!
	var status : String!
	var subject : String!
	var unitNumber : String!
	var updatedAt : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		id = dictionary["_id"] as? String == nil ? "" : dictionary["_id"] as? String
		index = dictionary["_index"] as? String == nil ? "" : dictionary["_index"] as? String
		score = dictionary["_score"] as? String == nil ? "" : dictionary["_score"] as? String
		type = dictionary["_type"] as? String == nil ? "" : dictionary["_type"] as? String
		complaintCode = dictionary["complaint_code"] as? String == nil ? "" : dictionary["complaint_code"] as? String
		if let complaintTypeData = dictionary["complaint_type"] as? NSDictionary{
			complaintType = SRListComplaintsComplaintType(fromDictionary: complaintTypeData)
		}
		else
		{
			complaintType = SRListComplaintsComplaintType(fromDictionary: NSDictionary.init())
		}
		complaintTypeId = dictionary["complaint_type_id"] as? String == nil ? "" : dictionary["complaint_type_id"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		identityId = dictionary["identity_id"] as? Int == nil ? 0 : dictionary["identity_id"] as? Int
		if let residentData = dictionary["resident"] as? NSDictionary{
			resident = SRListComplaintsResident(fromDictionary: residentData)
		}
		else
		{
			resident = SRListComplaintsResident(fromDictionary: NSDictionary.init())
		}
		residentUnitId = dictionary["resident_unit_id"] as? String == nil ? "" : dictionary["resident_unit_id"] as? String
		resolvedAt = dictionary["resolved_at"] as? String == nil ? "" : dictionary["resolved_at"] as? String
		resolvedBy = dictionary["resolved_by"] as? String == nil ? "" : dictionary["resolved_by"] as? String
		sort = dictionary["sort"] as? [Int] == nil ? [] : dictionary["sort"] as? [Int]
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		subject = dictionary["subject"] as? String == nil ? "" : dictionary["subject"] as? String
		unitNumber = dictionary["unit_number"] as? String == nil ? "" : dictionary["unit_number"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["_id"] = id
		}
		if index != nil{
			dictionary["_index"] = index
		}
		if score != nil{
			dictionary["_score"] = score
		}
		if type != nil{
			dictionary["_type"] = type
		}
		if complaintCode != nil{
			dictionary["complaint_code"] = complaintCode
		}
		if complaintType != nil{
			dictionary["complaint_type"] = complaintType.toDictionary()
		}
		if complaintTypeId != nil{
			dictionary["complaint_type_id"] = complaintTypeId
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if identityId != nil{
			dictionary["identity_id"] = identityId
		}
		if resident != nil{
			dictionary["resident"] = resident.toDictionary()
		}
		if residentUnitId != nil{
			dictionary["resident_unit_id"] = residentUnitId
		}
		if resolvedAt != nil{
			dictionary["resolved_at"] = resolvedAt
		}
		if resolvedBy != nil{
			dictionary["resolved_by"] = resolvedBy
		}
		if sort != nil{
			dictionary["sort"] = sort
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subject != nil{
			dictionary["subject"] = subject
		}
		if unitNumber != nil{
			dictionary["unit_number"] = unitNumber
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "_id") as? String
         index = aDecoder.decodeObject(forKey: "_index") as? String
         score = aDecoder.decodeObject(forKey: "_score") as? String
         type = aDecoder.decodeObject(forKey: "_type") as? String
         complaintCode = aDecoder.decodeObject(forKey: "complaint_code") as? String
         complaintType = aDecoder.decodeObject(forKey: "complaint_type") as? SRListComplaintsComplaintType
         complaintTypeId = aDecoder.decodeObject(forKey: "complaint_type_id") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         identityId = aDecoder.decodeObject(forKey: "identity_id") as? Int
         resident = aDecoder.decodeObject(forKey: "resident") as? SRListComplaintsResident
         residentUnitId = aDecoder.decodeObject(forKey: "resident_unit_id") as? String
         resolvedAt = aDecoder.decodeObject(forKey: "resolved_at") as? String
         resolvedBy = aDecoder.decodeObject(forKey: "resolved_by") as? String
         sort = aDecoder.decodeObject(forKey: "sort") as? [Int]
         status = aDecoder.decodeObject(forKey: "status") as? String
         subject = aDecoder.decodeObject(forKey: "subject") as? String
         unitNumber = aDecoder.decodeObject(forKey: "unit_number") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if id != nil{
			aCoder.encode(id, forKey: "_id")
		}
		if index != nil{
			aCoder.encode(index, forKey: "_index")
		}
		if score != nil{
			aCoder.encode(score, forKey: "_score")
		}
		if type != nil{
			aCoder.encode(type, forKey: "_type")
		}
		if complaintCode != nil{
			aCoder.encode(complaintCode, forKey: "complaint_code")
		}
		if complaintType != nil{
			aCoder.encode(complaintType, forKey: "complaint_type")
		}
		if complaintTypeId != nil{
			aCoder.encode(complaintTypeId, forKey: "complaint_type_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if identityId != nil{
			aCoder.encode(identityId, forKey: "identity_id")
		}
		if resident != nil{
			aCoder.encode(resident, forKey: "resident")
		}
		if residentUnitId != nil{
			aCoder.encode(residentUnitId, forKey: "resident_unit_id")
		}
		if resolvedAt != nil{
			aCoder.encode(resolvedAt, forKey: "resolved_at")
		}
		if resolvedBy != nil{
			aCoder.encode(resolvedBy, forKey: "resolved_by")
		}
		if sort != nil{
			aCoder.encode(sort, forKey: "sort")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if subject != nil{
			aCoder.encode(subject, forKey: "subject")
		}
		if unitNumber != nil{
			aCoder.encode(unitNumber, forKey: "unit_number")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}
