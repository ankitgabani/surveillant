//
//	FamilyMembersData.swift
//
//	Create by mac on 22/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FamilyMembersData : NSObject, NSCoding{

	var category : String!
	var createdAt : String!
	var email : String!
	var id : String!
	var name : String!
	var phone : String!
	var status : String!
	var unitId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		category = dictionary["category"] as? String == nil ? "" : dictionary["category"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		unitId = dictionary["unit_id"] as? String == nil ? "" : dictionary["unit_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if category != nil{
			dictionary["category"] = category
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if email != nil{
			dictionary["email"] = email
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if status != nil{
			dictionary["status"] = status
		}
		if unitId != nil{
			dictionary["unit_id"] = unitId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         category = aDecoder.decodeObject(forKey: "category") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         unitId = aDecoder.decodeObject(forKey: "unit_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if unitId != nil{
			aCoder.encode(unitId, forKey: "unit_id")
		}

	}

}