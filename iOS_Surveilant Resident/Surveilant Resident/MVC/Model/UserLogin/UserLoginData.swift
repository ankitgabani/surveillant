//
//	UserLoginData.swift
//
//	Create by Mac M1 on 17/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserLoginData : NSObject, NSCoding{

	var countryCode : String!
	var createdAt : String!
	var email : String!
	var emailVerified : Bool!
	var id : String!
	var lastLogin : String!
	var name : String!
	var phone : String!
	var phoneVerified : Bool!
	var resetField : Bool!
	var status : String!
	var token : String!
	var updatedAt : String!

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		countryCode = dictionary["country_code"] as? String == nil ? "" : dictionary["country_code"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		emailVerified = dictionary["email_verified"] as? Bool == nil ? false : dictionary["email_verified"] as? Bool
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		lastLogin = dictionary["last_login"] as? String == nil ? "" : dictionary["last_login"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		phoneVerified = dictionary["phone_verified"] as? Bool == nil ? false : dictionary["phone_verified"] as? Bool
		resetField = dictionary["reset_field"] as? Bool == nil ? false : dictionary["reset_field"] as? Bool
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		token = dictionary["token"] as? String == nil ? "" : dictionary["token"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if countryCode != nil{
			dictionary["country_code"] = countryCode
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if email != nil{
			dictionary["email"] = email
		}
		if emailVerified != nil{
			dictionary["email_verified"] = emailVerified
		}
		if id != nil{
			dictionary["id"] = id
		}
		if lastLogin != nil{
			dictionary["last_login"] = lastLogin
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if phoneVerified != nil{
			dictionary["phone_verified"] = phoneVerified
		}
		if resetField != nil{
			dictionary["reset_field"] = resetField
		}
		if status != nil{
			dictionary["status"] = status
		}
		if token != nil{
			dictionary["token"] = token
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryCode = aDecoder.decodeObject(forKey: "country_code") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         emailVerified = aDecoder.decodeObject(forKey: "email_verified") as? Bool
         id = aDecoder.decodeObject(forKey: "id") as? String
         lastLogin = aDecoder.decodeObject(forKey: "last_login") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         phoneVerified = aDecoder.decodeObject(forKey: "phone_verified") as? Bool
         resetField = aDecoder.decodeObject(forKey: "reset_field") as? Bool
         status = aDecoder.decodeObject(forKey: "status") as? String
         token = aDecoder.decodeObject(forKey: "token") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if countryCode != nil{
			aCoder.encode(countryCode, forKey: "country_code")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if emailVerified != nil{
			aCoder.encode(emailVerified, forKey: "email_verified")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lastLogin != nil{
			aCoder.encode(lastLogin, forKey: "last_login")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if phoneVerified != nil{
			aCoder.encode(phoneVerified, forKey: "phone_verified")
		}
		if resetField != nil{
			aCoder.encode(resetField, forKey: "reset_field")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}
