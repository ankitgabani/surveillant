//
//	VisitorTypesDataData.swift
//
//	Create by mac on 22/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class VisitorTypesDataData : NSObject, NSCoding{

	var aliasName : String!
	var name : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		aliasName = dictionary["alias_name"] as? String == nil ? "" : dictionary["alias_name"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aliasName != nil{
			dictionary["alias_name"] = aliasName
		}
		if name != nil{
			dictionary["name"] = name
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aliasName = aDecoder.decodeObject(forKey: "alias_name") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if aliasName != nil{
			aCoder.encode(aliasName, forKey: "alias_name")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}

	}

}