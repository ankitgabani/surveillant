//
//	SRComplaintChatData.swift
//
//	Create by Mac M1 on 24/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRComplaintChatData : NSObject, NSCoding{

	var complaintCode : String!
	var complaintConversations : [SRComplaintChatComplaintConversation]!
	var complaintType : SRComplaintChatAuthor!
	var complaintTypeId : String!
	var createdAt : String!
	var descriptionField : String!
	var id : String!
	var resolvedAt : String!
	var resolvedBy : String!
	var status : String!
	var subject : String!
	var updatedAt : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		complaintCode = dictionary["complaint_code"] as? String == nil ? "" : dictionary["complaint_code"] as? String
		complaintConversations = [SRComplaintChatComplaintConversation]()
		if let complaintConversationsArray = dictionary["complaint_conversations"] as? [NSDictionary]{
			for dic in complaintConversationsArray{
				let value = SRComplaintChatComplaintConversation(fromDictionary: dic)
				complaintConversations.append(value)
			}
		}
		if let complaintTypeData = dictionary["complaint_type"] as? NSDictionary{
			complaintType = SRComplaintChatAuthor(fromDictionary: complaintTypeData)
		}
		else
		{
			complaintType = SRComplaintChatAuthor(fromDictionary: NSDictionary.init())
		}
		complaintTypeId = dictionary["complaint_type_id"] as? String == nil ? "" : dictionary["complaint_type_id"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		resolvedAt = dictionary["resolved_at"] as? String == nil ? "" : dictionary["resolved_at"] as? String
		resolvedBy = dictionary["resolved_by"] as? String == nil ? "" : dictionary["resolved_by"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		subject = dictionary["subject"] as? String == nil ? "" : dictionary["subject"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if complaintCode != nil{
			dictionary["complaint_code"] = complaintCode
		}
		if complaintConversations != nil{
			var dictionaryElements = [NSDictionary]()
			for complaintConversationsElement in complaintConversations {
				dictionaryElements.append(complaintConversationsElement.toDictionary())
			}
			dictionary["complaint_conversations"] = dictionaryElements
		}
		if complaintType != nil{
			dictionary["complaint_type"] = complaintType.toDictionary()
		}
		if complaintTypeId != nil{
			dictionary["complaint_type_id"] = complaintTypeId
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if id != nil{
			dictionary["id"] = id
		}
		if resolvedAt != nil{
			dictionary["resolved_at"] = resolvedAt
		}
		if resolvedBy != nil{
			dictionary["resolved_by"] = resolvedBy
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subject != nil{
			dictionary["subject"] = subject
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         complaintCode = aDecoder.decodeObject(forKey: "complaint_code") as? String
         complaintConversations = aDecoder.decodeObject(forKey: "complaint_conversations") as? [SRComplaintChatComplaintConversation]
         complaintType = aDecoder.decodeObject(forKey: "complaint_type") as? SRComplaintChatAuthor
         complaintTypeId = aDecoder.decodeObject(forKey: "complaint_type_id") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         resolvedAt = aDecoder.decodeObject(forKey: "resolved_at") as? String
         resolvedBy = aDecoder.decodeObject(forKey: "resolved_by") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         subject = aDecoder.decodeObject(forKey: "subject") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if complaintCode != nil{
			aCoder.encode(complaintCode, forKey: "complaint_code")
		}
		if complaintConversations != nil{
			aCoder.encode(complaintConversations, forKey: "complaint_conversations")
		}
		if complaintType != nil{
			aCoder.encode(complaintType, forKey: "complaint_type")
		}
		if complaintTypeId != nil{
			aCoder.encode(complaintTypeId, forKey: "complaint_type_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if resolvedAt != nil{
			aCoder.encode(resolvedAt, forKey: "resolved_at")
		}
		if resolvedBy != nil{
			aCoder.encode(resolvedBy, forKey: "resolved_by")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if subject != nil{
			aCoder.encode(subject, forKey: "subject")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}