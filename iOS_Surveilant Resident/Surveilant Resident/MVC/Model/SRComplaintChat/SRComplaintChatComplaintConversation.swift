//
//	SRComplaintChatComplaintConversation.swift
//
//	Create by Mac M1 on 24/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRComplaintChatComplaintConversation : NSObject, NSCoding{

	var author : SRComplaintChatAuthor!
	var authorType : String!
	var content : String!
	var createdAt : String!
	var id : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		if let authorData = dictionary["author"] as? NSDictionary{
			author = SRComplaintChatAuthor(fromDictionary: authorData)
		}
		else
		{
			author = SRComplaintChatAuthor(fromDictionary: NSDictionary.init())
		}
		authorType = dictionary["author_type"] as? String == nil ? "" : dictionary["author_type"] as? String
		content = dictionary["content"] as? String == nil ? "" : dictionary["content"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if author != nil{
			dictionary["author"] = author.toDictionary()
		}
		if authorType != nil{
			dictionary["author_type"] = authorType
		}
		if content != nil{
			dictionary["content"] = content
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         author = aDecoder.decodeObject(forKey: "author") as? SRComplaintChatAuthor
         authorType = aDecoder.decodeObject(forKey: "author_type") as? String
         content = aDecoder.decodeObject(forKey: "content") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if author != nil{
			aCoder.encode(author, forKey: "author")
		}
		if authorType != nil{
			aCoder.encode(authorType, forKey: "author_type")
		}
		if content != nil{
			aCoder.encode(content, forKey: "content")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}