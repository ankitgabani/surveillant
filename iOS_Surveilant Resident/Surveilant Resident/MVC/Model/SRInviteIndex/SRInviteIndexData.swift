//
//	SRInviteIndexData.swift
//
//	Create by mac on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInviteIndexData : NSObject, NSCoding{

	var id : String!
	var index : String!
	var score : String!
	var type : String!
	var createdAt : String!
	var invitees : [SRInviteIndexInvitee]!
	var invitor : SRInviteIndexInvitor!
	var purpose : String!
	var remarks : String!
	var residentUnitId : String!
	var sort : [Int]!
	var state : String!
	var status : String!
	var updatedAt : String!
	var visitingTime : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		id = dictionary["_id"] as? String == nil ? "" : dictionary["_id"] as? String
		index = dictionary["_index"] as? String == nil ? "" : dictionary["_index"] as? String
		score = dictionary["_score"] as? String == nil ? "" : dictionary["_score"] as? String
		type = dictionary["_type"] as? String == nil ? "" : dictionary["_type"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		invitees = [SRInviteIndexInvitee]()
		if let inviteesArray = dictionary["invitees"] as? [NSDictionary]{
			for dic in inviteesArray{
				let value = SRInviteIndexInvitee(fromDictionary: dic)
				invitees.append(value)
			}
		}
		if let invitorData = dictionary["invitor"] as? NSDictionary{
			invitor = SRInviteIndexInvitor(fromDictionary: invitorData)
		}
		else
		{
			invitor = SRInviteIndexInvitor(fromDictionary: NSDictionary.init())
		}
		purpose = dictionary["purpose"] as? String == nil ? "" : dictionary["purpose"] as? String
		remarks = dictionary["remarks"] as? String == nil ? "" : dictionary["remarks"] as? String
		residentUnitId = dictionary["resident_unit_id"] as? String == nil ? "" : dictionary["resident_unit_id"] as? String
		sort = dictionary["sort"] as? [Int] == nil ? [] : dictionary["sort"] as? [Int]
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
		visitingTime = dictionary["visiting_time"] as? String == nil ? "" : dictionary["visiting_time"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["_id"] = id
		}
		if index != nil{
			dictionary["_index"] = index
		}
		if score != nil{
			dictionary["_score"] = score
		}
		if type != nil{
			dictionary["_type"] = type
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if invitees != nil{
			var dictionaryElements = [NSDictionary]()
			for inviteesElement in invitees {
				dictionaryElements.append(inviteesElement.toDictionary())
			}
			dictionary["invitees"] = dictionaryElements
		}
		if invitor != nil{
			dictionary["invitor"] = invitor.toDictionary()
		}
		if purpose != nil{
			dictionary["purpose"] = purpose
		}
		if remarks != nil{
			dictionary["remarks"] = remarks
		}
		if residentUnitId != nil{
			dictionary["resident_unit_id"] = residentUnitId
		}
		if sort != nil{
			dictionary["sort"] = sort
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if visitingTime != nil{
			dictionary["visiting_time"] = visitingTime
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "_id") as? String
         index = aDecoder.decodeObject(forKey: "_index") as? String
         score = aDecoder.decodeObject(forKey: "_score") as? String
         type = aDecoder.decodeObject(forKey: "_type") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         invitees = aDecoder.decodeObject(forKey: "invitees") as? [SRInviteIndexInvitee]
         invitor = aDecoder.decodeObject(forKey: "invitor") as? SRInviteIndexInvitor
         purpose = aDecoder.decodeObject(forKey: "purpose") as? String
         remarks = aDecoder.decodeObject(forKey: "remarks") as? String
         residentUnitId = aDecoder.decodeObject(forKey: "resident_unit_id") as? String
         sort = aDecoder.decodeObject(forKey: "sort") as? [Int]
         state = aDecoder.decodeObject(forKey: "state") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         visitingTime = aDecoder.decodeObject(forKey: "visiting_time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if id != nil{
			aCoder.encode(id, forKey: "_id")
		}
		if index != nil{
			aCoder.encode(index, forKey: "_index")
		}
		if score != nil{
			aCoder.encode(score, forKey: "_score")
		}
		if type != nil{
			aCoder.encode(type, forKey: "_type")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if invitees != nil{
			aCoder.encode(invitees, forKey: "invitees")
		}
		if invitor != nil{
			aCoder.encode(invitor, forKey: "invitor")
		}
		if purpose != nil{
			aCoder.encode(purpose, forKey: "purpose")
		}
		if remarks != nil{
			aCoder.encode(remarks, forKey: "remarks")
		}
		if residentUnitId != nil{
			aCoder.encode(residentUnitId, forKey: "resident_unit_id")
		}
		if sort != nil{
			aCoder.encode(sort, forKey: "sort")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if visitingTime != nil{
			aCoder.encode(visitingTime, forKey: "visiting_time")
		}

	}

}
