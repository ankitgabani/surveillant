//
//	SRInviteIndex.swift
//
//	Create by mac on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInviteIndex : NSObject, NSCoding{

	var aggregation : SRInviteIndexAggregation!
	var data : [SRInviteIndexData]!
	var status : Bool!
	var totalEntries : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		if let aggregationData = dictionary["aggregation"] as? NSDictionary{
			aggregation = SRInviteIndexAggregation(fromDictionary: aggregationData)
		}
		else
		{
			aggregation = SRInviteIndexAggregation(fromDictionary: NSDictionary.init())
		}
		data = [SRInviteIndexData]()
		if let dataArray = dictionary["data"] as? [NSDictionary]{
			for dic in dataArray{
				let value = SRInviteIndexData(fromDictionary: dic)
				data.append(value)
			}
		}
		status = dictionary["status"] as? Bool == nil ? false : dictionary["status"] as? Bool
		totalEntries = dictionary["total_entries"] as? Int == nil ? 0 : dictionary["total_entries"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aggregation != nil{
			dictionary["aggregation"] = aggregation.toDictionary()
		}
		if data != nil{
			var dictionaryElements = [NSDictionary]()
			for dataElement in data {
				dictionaryElements.append(dataElement.toDictionary())
			}
			dictionary["data"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		if totalEntries != nil{
			dictionary["total_entries"] = totalEntries
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aggregation = aDecoder.decodeObject(forKey: "aggregation") as? SRInviteIndexAggregation
         data = aDecoder.decodeObject(forKey: "data") as? [SRInviteIndexData]
         status = aDecoder.decodeObject(forKey: "status") as? Bool
         totalEntries = aDecoder.decodeObject(forKey: "total_entries") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if aggregation != nil{
			aCoder.encode(aggregation, forKey: "aggregation")
		}
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if totalEntries != nil{
			aCoder.encode(totalEntries, forKey: "total_entries")
		}

	}

}