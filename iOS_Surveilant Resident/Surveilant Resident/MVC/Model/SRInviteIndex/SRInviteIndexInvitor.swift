//
//	SRInviteIndexInvitor.swift
//
//	Create by mac on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInviteIndexInvitor : NSObject, NSCoding{

	var countryCode : String!
	var id : String!
	var name : String!
	var phone : String!
	var unitNumber : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		countryCode = dictionary["country_code"] as? String == nil ? "" : dictionary["country_code"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		unitNumber = dictionary["unit_number"] as? String == nil ? "" : dictionary["unit_number"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if countryCode != nil{
			dictionary["country_code"] = countryCode
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if unitNumber != nil{
			dictionary["unit_number"] = unitNumber
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryCode = aDecoder.decodeObject(forKey: "country_code") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         unitNumber = aDecoder.decodeObject(forKey: "unit_number") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if countryCode != nil{
			aCoder.encode(countryCode, forKey: "country_code")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if unitNumber != nil{
			aCoder.encode(unitNumber, forKey: "unit_number")
		}

	}

}