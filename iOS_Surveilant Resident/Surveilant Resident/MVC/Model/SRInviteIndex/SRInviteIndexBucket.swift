//
//	SRInviteIndexBucket.swift
//
//	Create by mac on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInviteIndexBucket : NSObject, NSCoding{

	var docCount : Int!
	var key : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		docCount = dictionary["doc_count"] as? Int == nil ? 0 : dictionary["doc_count"] as? Int
		key = dictionary["key"] as? String == nil ? "" : dictionary["key"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if docCount != nil{
			dictionary["doc_count"] = docCount
		}
		if key != nil{
			dictionary["key"] = key
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         docCount = aDecoder.decodeObject(forKey: "doc_count") as? Int
         key = aDecoder.decodeObject(forKey: "key") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if docCount != nil{
			aCoder.encode(docCount, forKey: "doc_count")
		}
		if key != nil{
			aCoder.encode(key, forKey: "key")
		}

	}

}