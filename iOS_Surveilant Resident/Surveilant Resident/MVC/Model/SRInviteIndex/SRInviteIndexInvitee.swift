//
//	SRInviteIndexInvitee.swift
//
//	Create by mac on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInviteIndexInvitee : NSObject, NSCoding{

	var countryCode : Int!
	var id : String!
	var inviteUrl : String!
	var name : String!
	var phone : String!
	var status : String!
	var vehicleNumber : String!
	var visitedAt : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		countryCode = dictionary["country_code"] as? Int == nil ? 0 : dictionary["country_code"] as? Int
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		inviteUrl = dictionary["invite_url"] as? String == nil ? "" : dictionary["invite_url"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		vehicleNumber = dictionary["vehicle_number"] as? String == nil ? "" : dictionary["vehicle_number"] as? String
		visitedAt = dictionary["visited_at"] as? String == nil ? "" : dictionary["visited_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if countryCode != nil{
			dictionary["country_code"] = countryCode
		}
		if id != nil{
			dictionary["id"] = id
		}
		if inviteUrl != nil{
			dictionary["invite_url"] = inviteUrl
		}
		if name != nil{
			dictionary["name"] = name
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if status != nil{
			dictionary["status"] = status
		}
		if vehicleNumber != nil{
			dictionary["vehicle_number"] = vehicleNumber
		}
		if visitedAt != nil{
			dictionary["visited_at"] = visitedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryCode = aDecoder.decodeObject(forKey: "country_code") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         inviteUrl = aDecoder.decodeObject(forKey: "invite_url") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         vehicleNumber = aDecoder.decodeObject(forKey: "vehicle_number") as? String
         visitedAt = aDecoder.decodeObject(forKey: "visited_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if countryCode != nil{
			aCoder.encode(countryCode, forKey: "country_code")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if inviteUrl != nil{
			aCoder.encode(inviteUrl, forKey: "invite_url")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if vehicleNumber != nil{
			aCoder.encode(vehicleNumber, forKey: "vehicle_number")
		}
		if visitedAt != nil{
			aCoder.encode(visitedAt, forKey: "visited_at")
		}

	}

}