//
//	SRInviteIndexState.swift
//
//	Create by mac on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInviteIndexState : NSObject, NSCoding{

	var buckets : [SRInviteIndexBucket]!
	var docCountErrorUpperBound : Int!
	var sumOtherDocCount : Int!
	var docCount : Int!
	var state : SRInviteIndexState!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		buckets = [SRInviteIndexBucket]()
		if let bucketsArray = dictionary["buckets"] as? [NSDictionary]{
			for dic in bucketsArray{
				let value = SRInviteIndexBucket(fromDictionary: dic)
				buckets.append(value)
			}
		}
		docCountErrorUpperBound = dictionary["doc_count_error_upper_bound"] as? Int == nil ? 0 : dictionary["doc_count_error_upper_bound"] as? Int
		sumOtherDocCount = dictionary["sum_other_doc_count"] as? Int == nil ? 0 : dictionary["sum_other_doc_count"] as? Int
		docCount = dictionary["doc_count"] as? Int == nil ? 0 : dictionary["doc_count"] as? Int
		if let stateData = dictionary["state"] as? NSDictionary{
			state = SRInviteIndexState(fromDictionary: stateData)
		}
		else
		{
			state = SRInviteIndexState(fromDictionary: NSDictionary.init())
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if buckets != nil{
			var dictionaryElements = [NSDictionary]()
			for bucketsElement in buckets {
				dictionaryElements.append(bucketsElement.toDictionary())
			}
			dictionary["buckets"] = dictionaryElements
		}
		if docCountErrorUpperBound != nil{
			dictionary["doc_count_error_upper_bound"] = docCountErrorUpperBound
		}
		if sumOtherDocCount != nil{
			dictionary["sum_other_doc_count"] = sumOtherDocCount
		}
		if docCount != nil{
			dictionary["doc_count"] = docCount
		}
		if state != nil{
			dictionary["state"] = state.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         buckets = aDecoder.decodeObject(forKey: "buckets") as? [SRInviteIndexBucket]
         docCountErrorUpperBound = aDecoder.decodeObject(forKey: "doc_count_error_upper_bound") as? Int
         sumOtherDocCount = aDecoder.decodeObject(forKey: "sum_other_doc_count") as? Int
         docCount = aDecoder.decodeObject(forKey: "doc_count") as? Int
         state = aDecoder.decodeObject(forKey: "state") as? SRInviteIndexState

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if buckets != nil{
			aCoder.encode(buckets, forKey: "buckets")
		}
		if docCountErrorUpperBound != nil{
			aCoder.encode(docCountErrorUpperBound, forKey: "doc_count_error_upper_bound")
		}
		if sumOtherDocCount != nil{
			aCoder.encode(sumOtherDocCount, forKey: "sum_other_doc_count")
		}
		if docCount != nil{
			aCoder.encode(docCount, forKey: "doc_count")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}

	}

}