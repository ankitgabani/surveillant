//
//	SRInviteIndexAggregation.swift
//
//	Create by mac on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SRInviteIndexAggregation : NSObject, NSCoding{

	var state : SRInviteIndexState!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		if let stateData = dictionary["state"] as? NSDictionary{
			state = SRInviteIndexState(fromDictionary: stateData)
		}
		else
		{
			state = SRInviteIndexState(fromDictionary: NSDictionary.init())
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if state != nil{
			dictionary["state"] = state.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         state = aDecoder.decodeObject(forKey: "state") as? SRInviteIndexState

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}

	}

}