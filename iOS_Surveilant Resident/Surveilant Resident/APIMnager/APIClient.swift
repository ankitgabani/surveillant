//
//  APIClient.swift
//  Surveilant Resident
//
//  Created by Gabani King on 13/08/21.
//

import Foundation
import Alamofire
import SVProgressHUD
import UIKit

class APIClient: NSObject {
    
    class var sharedInstance: APIClient {
        
        struct Static {
            static let instance: APIClient = APIClient()
        }
        return Static.instance
    }
    
    var responseData: NSMutableData!
    
    func showLogoutAlert(completion:@escaping ((_ tapped:Bool)->Void))
    {
        AppUtilites.showAlert(title: "No Network Found!", message: "", cancelButtonTitle: "OK")
    }
    
    
    func MakeAPICallWithAuthHeaderPutClose(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            let device_id = UIDevice.current.identifierForVendor!.uuidString
            
            var headers: HTTPHeaders = ["" : ""]
            
            let token = appDelagte.dicLoginUserDetails?.token ?? ""

            headers = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Authorization": "JWT \(token)"]
            
            print("Header = \(headers)")
            
            AF.request(BASE_URL + url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                
                case .success:
                    if response.value != nil{
                        
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    func MakeAPICallWithAuthHeaderPut(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            let device_id = UIDevice.current.identifierForVendor!.uuidString
            
            var headers: HTTPHeaders = ["" : ""]
            
            if url == OTP_VERIFY || url == VALIDATE_FORGOT_PASSWORD_OTP || url == RESET_PASSWORD || url == VERIFY_REGISTRATION
            {
                let token_ = UserDefaults.standard.value(forKey: "AuthorizationTOKEN") as? String
                
                headers = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Device-Id": device_id,"Authorization": "base64 \(token_ ?? "")"]
             
            }
            else if url == SET_PASSWORD
            {
                let token = appDelagte.dicLoginUserDetails?.token ?? ""

                headers = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Device-Id": device_id,"Authorization": "JWT \(token)"]

            }
            else
            {
                headers = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Device-Id": device_id]
            }
            
            print("Header = \(headers)")
            
            AF.request(BASE_URL + url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                
                case .success:
                    if response.value != nil{
                        
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    
    func MakeAPICallWithAuthHeaderTokenPut(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            let device_id = UIDevice.current.identifierForVendor!.uuidString
            let token = appDelagte.dicLoginUserDetails?.token ?? ""

            
            let headers: HTTPHeaders = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Authorization": "JWT \(token)"]
            
            print("Header = \(headers)")
            
            AF.request(BASE_URL + url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                
                case .success:
                    if response.value != nil{
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    func MakeAPICallWithAuthHeaderGET(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            let token = appDelagte.dicLoginUserDetails?.token ?? ""
            
            let headers: HTTPHeaders = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Authorization": "JWT \(token)"]
            
            print("Header = \(headers)")
            
            AF.request(BASE_URL + url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                
                case .success:
                    if response.value != nil{
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
        
    func MakeAPICallWithAuthHeaderPOST(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            let token = appDelagte.dicLoginUserDetails?.token ?? ""
            
            let headers: HTTPHeaders = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Authorization": "JWT \(token)"]
            
            print("Header = \(headers)")
            
            AF.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                
                case .success:
                    if response.value != nil{
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    
    func MakeAPICallWithAuthHeaderDELETE(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            let token = appDelagte.dicLoginUserDetails?.token ?? ""
            
            let headers: HTTPHeaders = ["X-App-Version" : appVersion!,"Request-Source":"iOS","Authorization": "JWT \(token)"]
            
            print("Header = \(headers)")
            
            AF.request(BASE_URL + url, method: .delete, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                
                case .success:
                    if response.value != nil{
                        
                        if let responseDict = ((response.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            showLogoutAlert { (true) in
                
            }
            // pushNetworkErrorVC()
            SVProgressHUD.dismiss()
        }
    }
    
    func showIndicator(){
        SVProgressHUD.show()
    }
    
    func hideIndicator(){
        SVProgressHUD.dismiss()
    }
    
    func showSuccessIndicator(message: String){
        SVProgressHUD.showSuccess(withStatus: message)
    }
}

