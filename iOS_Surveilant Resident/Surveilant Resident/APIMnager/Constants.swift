//
//  Constants.swift
//  Surveilant Resident
//
//  Created by Gabani King on 13/08/21.
//

import Foundation
import UIKit

let appDelagte = UIApplication.shared.delegate as! AppDelegate

var isFromRegister = false

var ROZER_API_KEY = "rzp_test_gDuXv0kCEVsLkn"

//lokesh@katomaran.com
//p25JenhbnHQ8jZ4

//MARK:- BASE_URL
let BASE_URL = "https://api-stage.surveillant.ai/"


//MARK:- Authentication
let REGISTER_RESIDENT = "v1/residents/register"

let LOGIN_RESIDENT = "v1/residents/login"

let SEND_OTP = "v1/residents/send_otp"

let OTP_VERIFY = "v1/residents/verify_otp"

let FORGOT_PASSWORD = "v1/residents/forgot_password"

let VALIDATE_FORGOT_PASSWORD_OTP = "v1/residents/validate_otp"

let RESET_PASSWORD = "v1/residents/reset_password"

let VERIFY_REGISTRATION = "v1/residents/verify_registration"

let SET_PASSWORD = "v1/residents/set_password"


//MARK:- Vehicle
let VEHICLE_LIST = "v1/residents/vehicles"

let CREATE_VEHICLE_LIST = "v1/residents/vehicles"

let DELETE_VEHICLE = "v1/residents/vehicles/{{vehicle_id}}/owner"


//MARK:- Family Members
let MEMBER_LIST = "v1/units/list_members"

let CREATE_MEMBER = "v1/units/add_member"

let DELETE_MEMBER = "v1/units/remove_member"



//MARK:- Notification
let NOTIFICATION_COUNT = "v1/residents/notifications/unread"

let NOTIFICATION_LIST = "v1/residents/notifications"

let NOTIFICATION_STATUS = "v1/residents/notifications/{{res_notification_id}}/status"

let NOTIFICATION_CLEAR = "v1/residents/notifications/clear"


//MARK:- Payment

let GET_PAYMENT = "v1/units/payments"

let ALL_PAYMENT = "v1/units/all_payments"

//let RAZORPAY_PAYMENT = "v1/residents/payment_units/{{payment_unit_id}}/razorpay_payment"


//MARK:- Invite

let VISITOR_TYPES = "v1/visitor_types"

let RESIDENTS_INVITE = "v1/residents/invites"

let CREATE_INVITE = "v1/residents/invites"



//MARK:- Society
let ALL_SOCIETY = "v1/societies"

let SOS_CONTACT_NO = "v1/societies/sos_contact_no"


//MARK:- Resident
let PROFILE_DETAILS = "v1/residents/profile"


//MARK:- Notice
let SHOW_PROFILE = "v1/residents/profile"

let RESIDENT_NOTICE = "v1/residents/notices"


//MARK:- City
//let ALL_CITY = "v1/cities"


//MARK:- Complaints
let CREATE_COMPLAINTS = "v1/residents/complaints"

let RESIDENT_REPLY = "v1/residents/complaints/{{complaint_id}}/conversations"

let LIST_COMPLAINTS = "v1/residents/complaints"

let SHOW_COMPLAINTS = "v1/residents/complaints/{{complaint_id}}"

let COMPLAINTS_TYPE_LIST = "v1/residents/complaints/type_list"
