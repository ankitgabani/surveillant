//
//  AppColor.swift
//  Swoosh Rider
//
//  Created by Gabani King on 01/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import Foundation
import UIKit

//#1181D9 Primary gradient-1
let PRIMARY_STATUSBAR_COLOR = UIColor(hexString: "2934D0")

let GRAY_COLOR = UIColor(hexString: "868686")

let BORDER_NORMAL_COLOR = UIColor(hexString: "E8E6EA")

let RED_COLOR = UIColor(hexString: "E64D2E")

let RED_Light_COLOR = UIColor(hexString: "FFECED")

let GREEN_COLOR = UIColor(hexString: "3FBF62")

let GREEN_Light_COLOR = UIColor(hexString: "EBF7EE")

let PISTA_COLOR = UIColor(hexString: "3BB1B9")

let PISTA_Light_COLOR = UIColor(hexString: "E4FDFF")



extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)

        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }

        var color: UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0

        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
}
