//
//  AppDelegate.swift
//  Surveilant Resident
//
//  Created by Gabani King on 06/08/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var dicLoginUserDetails: UserLoginData?
    var dicMainData = NSDictionary()
    var isResetPasswordProfile = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .lightContent
        
        sleep(2)
        self.dicLoginUserDetails = getCurrentUserData()
        
        knowDeviceType()

        if let isFinishedIntro = UserDefaults.standard.value(forKey: "isFinishedIntro") as? Bool
        {
            if isFinishedIntro == true
            {
                if let isUserLogin = UserDefaults.standard.value(forKey: "isUserLogin") as? Bool
                {
                    if isUserLogin == true
                    {
                        setUpHome()
                    }
                    else
                    {
                        setUpLogin()
                    }
                }
                else
                {
                    setUpLogin()
                }
            }
            else
            {
                setUpIntroll()
            }
        }
        else
        {
            setUpIntroll()
        }
        
        
        
        //   setUpHome()
        
        // Override point for customization after application launch.
        return true
    }
    
    func saveCurrentUserData(dic: UserLoginData)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "currentUserData")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> UserLoginData
    {
        if let data = UserDefaults.standard.object(forKey: "currentUserData"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! UserLoginData
        }
        return UserLoginData.init()
    }
    
    func knowDeviceType(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                
            case 1334:
                print("iPhone 6/6S/7/8")
                
            case 1792:
                print("iPhone XR/ 11")
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                
            case 2340:
                print("iphone 12 mini")
                
            case 2436:
                print("iPhone X/XS/11 Pro")
                
            case 2532:
                print("iphone 12 pro")
                
            case 2688:
                print("iPhone XS Max/11 Pro Max")
                
            case 2778:
                print("iphone 12 pro max")
                
                
            default:
                print("Unknown")
            }
        }
    }
    
    func setUpIntroll()
    {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "IntrollViewController") as! IntrollViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "IntrollViewController") as! IntrollViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func setUpLogin()
    {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "LoginSelectionVC") as! LoginSelectionVC
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "LoginSelectionVC") as! LoginSelectionVC
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func setUpHome()
    {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func setUpProfile()
    {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func setUpComplaint()
    {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintsViewController") as! ComplaintsViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "ComplaintsViewController") as! ComplaintsViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            }
        }
        
    }
    
    func setUpPayemnt()
    {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "SmallDevice", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            default:
                print("Unknown")
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home = mainStoryboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                let homeNavigation = UINavigationController(rootViewController: home)
                homeNavigation.navigationBar.isHidden = true
                self.window?.rootViewController = homeNavigation
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
}
